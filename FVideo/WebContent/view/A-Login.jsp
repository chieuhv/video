<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Đăng nhập</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="view/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="view/css/Style.css">
	<link rel="stylesheet" type="text/css" href="view/icon/css/all.css">
</head>
<body style="background-image:url(view/images/home.jpg);">
	<div class="container-fluid login">
		<form class="col-sm-5 mx-auto p-5 text-white bg-dark register" id = "" method="post" action="adminlogin">
			<div class="alert ${alert }">
				${message }
			</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label"> Tên đăng nhập</label>
	    		<div class="col-sm-8">
	      			<input type="text" class="form-control input" name="email" id="email" placeholder="Nhập email của bạn">
	      			<div class="text-danger" id = "mess1"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label"> Mật khẩu</label>
	    		<div class="col-sm-8">
	      			<input type="password" class="form-control input" name="password" id="password" placeholder="Nhập mật khẩu">
	      			<div class="text-danger" id = "mess2"></div>
	    		</div>
	  		</div>	  
	  		<div class="form-group row">
    			<div class="col-sm-4">
        			
    			</div>
    			<div class="col-sm-8">
        			<button class="btn btn-info" type="submit"><i class="fas fa-sign-in-alt"></i> Đăng nhập</button>
    			</div>
			</div>
		</form>
	</div>
</body>
</html>