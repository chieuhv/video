function Follow(follower, following){
	var xhttp = new XMLHttpRequest();
	
	var video = "follow"+following;
	var remove = "."+video;
	
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/addfollow?followerid="+follower+"&followingid="+following+"&accountid="+follower, true);
    xhttp.send();
    $(remove).remove();
}

function profileFollow(follower, following){
	var xhttp = new XMLHttpRequest();
	
	var video = "follow"+following;
	var remove = "."+video;
	var follow = "following"+following;
	var show = "."+follow;
	
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/addfollow?followerid="+follower+"&followingid="+following+"&accountid="+follower, true);
    xhttp.send();
    $(remove).hide();
    $(show).show();
}

function followingFollow(follower, following){
	var xhttp = new XMLHttpRequest();
	
	var unfo = "unfollow"+following;
	var fo = "follow"+following;
	
	var unfollow = "."+unfo;
	var foll = "."+fo;
	
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/addfollow?followerid="+follower+"&followingid="+following+"&accountid="+follower, true);
    xhttp.send();
    $(foll).hide();
    $(unfollow).show();
}

function unFollow(follower, following){
	var xhttp = new XMLHttpRequest();
	
	var unfo = "unfollow"+following;
	var fo = "follow"+following;
	
	var unfollow = "."+unfo;
	var foll = "."+fo;
		
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/deletefollow?follower="+follower+"&following="+following, true);
    xhttp.send();
    $(unfollow).hide();
    $(foll).show();
}

function profileUnfollow(follower, following){
var xhttp = new XMLHttpRequest();
	
	var video = "follow"+following;
	var remove = "."+video;
	var follow = "following"+following;
	var show = "."+follow;
	
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/deletefollow?follower="+follower+"&following="+following, true);
    xhttp.send();
    $(remove).show();
    $(show).hide();
}