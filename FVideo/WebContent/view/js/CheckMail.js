$(document).ready(function()
{
       	var form = $("#form");

        var email = $("#email");
        var mess1 = $("#mess1");
       	
       	email.blur(checkEmail);
       	email.keyup(checkEmail);
       	
       	var password = $("#password");
        var mess2 = $("#mess2");
        
        password.blur(checkPassword);
        password.keyup(checkPassword);

       	form.submit(function(){
            if(checkEmail() & checkPassword()){
               return true;
            }
            else{
            	return false;
            }
                
         });

    
    function checkEmail()
    {
    	var mail = $("#email").val();
    	var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    	if(!filter.test(mail))
    	{
    		email.addClass("error");
	        mess1.text("Nhập email không hợp lệ !");
	        mess1.addClass("error");
	        return false;
    	}
    	else
    	{
    		email.removeClass("error");
	        mess1.text("");
	        mess1.removeClass("error");
	        return true;
    	}
    }
    
    function checkPassword()
    {
    	var pass = $("#password").val();
    	if(pass.length<8)
    	{
    		password.addClass("error");
	        mess2.text("Nhập mật khẩu cũ của bạn !");
	        mess2.addClass("error");
	        return false;
    	}
    	else
    	{
    		password.removeClass("error");
	        mess2.text("");
	        mess2.removeClass("error");
	        return true;
    	}
    }

});

$(document).ready(function(){
    $('#back').click(function(){
    	parent.history.back();
    	return false;
    });
});