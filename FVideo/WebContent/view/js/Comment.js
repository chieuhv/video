function postComment(videoId,userId,idAccount){
	var xhttp = new XMLHttpRequest();
	var content = "content"+videoId;
	var vd = "videoId"+videoId;
	var us = "userId"+userId;
	var commentCount = "commentCount"+videoId;
	
	var con = document.getElementById(content).value;
	var video = document.getElementById(vd).value;
	var user = document.getElementById(us).value;
	if(con==""){
		return false;
	}
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("POST", window.location.origin+"/"+str[1]+"/video/addcomment", true);
	xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	document.getElementById(commentCount).innerHTML = Number(document.getElementById(commentCount).innerHTML) + 1;
	var txt = "";
	var linkImg = window.location.origin+"/"+str[1];
	var getname = "getname"+videoId;
	var getimg = "getimg"+videoId;
	var show = "show"+videoId;
	var remove = "remove"+videoId;
	var thumbnail = document.getElementById(getimg).innerHTML;
	var account = document.getElementById(getname).innerHTML;
	var node = document.createElement("div");
	node.className = "d-flex thumbnail p-1";
	 
	txt += "<div class=''>"+
	    "<a href='"+linkImg+"/video/profile?id="+userId+"'><img src='"+linkImg+"/"+thumbnail+"' class='img-circle'></a>"+
	    "</div>"+
	    "<div class='p-1'>"+
	    "<a href='"+linkImg+"/video/profile?id="+userId+"'>"+account+"</a>"+
	    "<div class='read-more'>"+con+" </div>"+
	    "<p class='text'>vừa xong</p>"+
	    "</div>"+
	    "</div>";
	node.innerHTML = txt;
	document.getElementById(show).appendChild(node);
	document.getElementById(content).value="";
    xhttp.send("account="+user+"&video="+video+"&content="+con+"&idaccount="+idAccount);
}

function pressComment(videoId,userId,idAccount)
{
	var xhttp = new XMLHttpRequest();
	
	var form = "formComment"+videoId;
	var show = "show"+videoId;
	document.getElementById(form).style.display="block";

	var check = "";
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/listcomment?id="+videoId, true);
	var linkImg = window.location.origin+"/"+str[1];
	xhttp.onreadystatechange = function() {
		var txt = "", x;
        if (this.readyState == 4 && this.status == 200) {
            var json = JSON.parse(this.responseText);
               for (x in json) {
            	  if(json[x].idAccount==userId || userId==idAccount){
            		  check="remove";
            	  }
            	  else{
            		  check = "notremove";
            	  }
                  txt += "<div class='d-flex thumbnail p-1' id='idcomment"+json[x].id+"'>" +
                  "<div class=''>"+
                  "<a href='"+linkImg+"/video/profile?id="+json[x].idAccount+"'><img src='"+linkImg+"/"+json[x].thumbnail+"' class='img-circle'></a>"+
                  "</div>"+
                  "<div class='p-1'>"+
                  "<a href='"+linkImg+"/video/profile?id="+json[x].idAccount+"'>"+json[x].accountName+"</a>"+
                  "<div class='read-more'>"+json[x].content+" <button class='"+check+" btn btn-light' type='submit' onclick='deleteComment("+json[x].id+","+json[x].idVideo+")'>Xóa</button></div>"+
                  "<p class='text'>"+json[x].date+" lúc "+json[x].time+"</p>"+
                  "</div>"+
                  "</div>";
            }
          	document.getElementById(show).innerHTML = txt;
        }
    };
    xhttp.send();
}

$(document).ready(function(){
    var submit = $("button[type='submit']");
    submit.click(function(event){
        event.preventDefault();
    });
});

function deleteComment(idComment,idVideo){
	var xhttp = new XMLHttpRequest();
	var idcomment = "idcomment"+idComment;
	var commentCount = "commentCount"+idVideo;
	var remove = document.getElementById(idcomment);
	$(remove).remove();
	document.getElementById(commentCount).innerHTML = Number(document.getElementById(commentCount).innerHTML) - 1;
	var path = window.location.pathname;
	var str= path.split("/");
	xhttp.open("GET", window.location.origin+"/"+str[1]+"/video/deletecomment?id="+idComment, true);
    xhttp.send();
}
/*
 * var idcmt = "comment"+id;
	var dl = document.getElementById(idcmt);
	$(dl).remove();
 */
/*[{"id":"1","idAccount":"2","idVideo":"4","date":"13-05-2020",
	"time":"20:21","content":"Vô tình gặp gỡ rồi mang theo nhiều mộng mơ",
	"accountName":"Văn Chiều","thumbnail":"upload/home.jpg"}]*/