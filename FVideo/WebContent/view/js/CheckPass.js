$(document).ready(function()
{
       	var form = $("#form");

        var email = $("#email");
        var mess1 = $("#mess1");

        var passold = $("#passold");
        var mess2 = $("#mess2");

        var passnew = $("#passnew");
        var mess3 = $("#mess3");
        
        email.blur(checkEmail);
       	email.keyup(checkEmail);
       	passold.blur(checkPassOld);
       	passnew.blur(checkPassNew);
       	passnew.keyup(checkPassNew);

       	form.submit(function(){
            if(checkEmail() & checkPassOld() & checkPassNew())
            {
               return true;
            }
            else
            {
            	return false;
            }
                
         });
       	
       	function checkEmail()
	    {
	    	var mail = $("#email").val();
	    	var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    	if(!filter.test(mail))
	    	{
	    		email.addClass("error");
		        mess1.text("Kiểm tra lại email của bạn !");
		        mess1.addClass("error");
		        return false;
	    	}
	    	else
	    	{
	    		email.removeClass("error");
		        mess1.text("");
		        mess1.removeClass("error");
		        return true;
	    	}
	    }

       	function checkPassOld()
        {
        	var passOld = $("#passold").val();
        	if(passOld.length==0)
        	{
        		passold.addClass("error");
    	        mess2.text("Nhập mật khẩu cũ của bạn !");
    	        mess2.addClass("error");
    	        return false;
        	}
        	else
        	{
        		passold.removeClass("error");
    	        mess2.text("");
    	        mess2.removeClass("error");
    	        return true;
        	}
        }
       	
       	function checkPassNew()
        {
        	var passNew = $("#passnew").val();
        	if(passNew.length<8)
        	{
        		passnew.addClass("error");
    	        mess3.text("Mật khẩu của bạn phải lớn hơn 8 ký tự !");
    	        mess3.addClass("error");
    	        return false;
        	}
        	else
        	{
        		passnew.removeClass("error");
    	        mess3.text("");
    	        mess3.removeClass("error");
    	        return true;
        	}
        }
	    
});