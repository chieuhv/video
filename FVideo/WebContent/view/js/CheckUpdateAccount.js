$(document).ready(function()
{
       	var form = $("#createAccount");

        var name = $("#name");
        var mess1 = $("#mess1");

        var date = $("#date");
        var mess3 = $("#mess3");

        var male = $("#male");
        var female = $("#female");
        var mess4 = $("#mess4");

        var email = $("#email");
        var mess5 = $("#mess5");

        var phone = $("#phone");
        var mess6 = $("#mess6");
        	
       	name.blur(checkName);
       	date.blur(checkDate);
       	male.blur(checkGender);
        female.blur(checkGender);
       	email.blur(checkEmail);
       	email.keyup(checkEmail);
       	phone.blur(checkPhone);

       	form.submit(function(){
            if(checkName() & checkDate() & checkGender() & checkEmail() & checkPhone()){
               return true;
            }
            else
                return false;
         });

    function checkName()
    {
        var fullname = $("#name").val();
        if(fullname.length == "")
        {
            name.addClass("error");
            mess1.text("Kiểm tra lại tên của bạn !");
            mess1.addClass("error");
            return false;
        }
        else
        {
            name.removeClass("error");
            mess1.text("");
            mess1.removeClass("error");
            return true;
        }
    }

    function checkDate()
    {
    	var birthDate = $("#date").val();
    	if(birthDate.length == "")
    	{
    		date.addClass("error");
	        mess3.text("Kiểm tra lại ngày sinh của bạn !");
	        mess3.addClass("error");
	        return false;
    	}
    	else
    	{
    		date.removeClass("error");
	        mess3.text("");
	        mess3.removeClass("error");
	        return true;
    	}
    }

    function checkGender()
    {
    	var sexmale = $("#male").val();
        var sexfemale = $("#female").val();
    	if(sexmale.checked==false && sexfemale.checked==false)
    	{
    		male.addClass("error");
            female.addClass("error");
	        mess4.text("Bạn chưa chọn giới tính nào !");
	        mess4.addClass("error");
	        return false;
    	}
    	else
    	{
    		male.removeClass("error");
            female.removeClass("error");
	        mess4.text("");
	        mess4.removeClass("error");
	        return true;
    	}
    }

    
    function checkEmail()
    {
    	var mail = $("#email").val();
    	var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    	if(!filter.test(mail))
    	{
    		email.addClass("error");
	        mess5.text("Kiểm tra lại email của bạn !");
	        mess5.addClass("error");
	        return false;
    	}
    	else
    	{
    		email.removeClass("error");
	        mess5.text("");
	        mess5.removeClass("error");
	        return true;
    	}
    }

    function checkPhone()
    {
    	var number = $("#phone").val();
    	var filter = /^[0-9]+$/;
    	if(!filter.test(number))
    	{
    		phone.addClass("error");
	        mess6.text("Kiểm tra lại số điện thoại của bạn !");
	        mess6.addClass("error");
	        return false;
    	}
    	else
    	{
    		phone.removeClass("error");
	        mess6.text("");
	        mess6.removeClass("error");
	        return true;
    	}
    }
});