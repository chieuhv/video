$(document).ready(function()
{
        var form = $("#updateVideo");

        var file = $("#videoUpload");
        var alert = $("#alert");
        file.blur(checkFile);

        form.submit(function(){
            if(checkFile()){
               return true;
            }
            else
            {
                return false;
            }
        });

    function checkFile()
    {
        var img = $("#videoUpload").val();
            var extension = img.split('.').pop().toLowerCase();
            if($.inArray(extension, ['mov', 'mp4', 'ogm']) == -1 && img != "")
            {
                file.addClass("error");
                alert.text("Vui lòng chọn video muốn tải lên !");
                alert.addClass("error");
                return false;
            }
            else
            {
                file.removeClass("error");
                alert.text("");
                alert.removeClass("error");
                return true;
            }
    }
});

$(function() {
		
    $("#videoUpload").change(function(){
    var reader = new FileReader();

    reader.onload = function(video){
    	$('#preview').attr('src', video.target.result);
    }
      	reader.readAsDataURL(this.files[0]);
      	
    });
});