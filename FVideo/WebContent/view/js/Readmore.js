$(document).ready(function() {
	var showChar = 100;
	var moretext = "Xem thêm";
	var lesstext = "Ẩn bớt";
	$('.read-more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var first = content.substr(0, showChar);
			var last = content.substr(showChar, content.length - showChar);

			var html = first + '<span class="moreellipses">...</span><span class="morecontent"><span>' 
			+ last + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});