$(document).ready(function()
{
        var form = $("#postVideo");

        var file = $("#file");
        var mess = $("#mess");
        file.blur(checkFile);

        form.submit(function(){
            if(checkFile()){
               return true;
            }
            else
                return false;
         });

    function checkFile()
    {
        var img = $("#file").val();
        var extension = img.split('.').pop().toLowerCase();
        if($.inArray(extension, ['mov', 'mp4', 'ogm']) == -1)
        {
            file.addClass("error");
            mess.text("Vui lòng chọn video muốn tải lên !");
            mess.addClass("error");
            return false;
        }
        else
        {
            file.removeClass("error");
            mess.text("");
            mess.removeClass("error");
            return true;
        }
    }
});

$(document).ready(function(){
    $('#back').click(function(){
    	parent.history.back();
    	return false;
    });
});