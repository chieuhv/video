$(document).ready(function()
{
       	var form = $("#createCode");

        var code = $("#code");
        var mess = $("#mess");
        	
       	code.blur(checkCode);
       	code.keyup(checkCode);

       	form.submit(function(){
            if(checkCode()){
               return true;
            }
            else
                return false;
         });

    function checkCode()
    {
    	var number = $("#code").val();
    	var filter = /^[0-9]+$/;
    	if(!filter.test(number) || number.length<6 || number.length>6)
    	{
    		code.addClass("error");
	        mess.text("Mã xác nhận phải có 6 ký tự !");
	        mess.addClass("error");
	        return false;
    	}
    	else
    	{
    		code.removeClass("error");
	        mess.text("");
	        mess.removeClass("error");
	        return true;
    	}
    }
});

$(document).ready(function(){
    $('#back').click(function(){
    	parent.history.back();
    	return false;
    });
});