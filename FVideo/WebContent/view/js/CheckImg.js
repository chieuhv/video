/*$(document).ready(function(){
  $("#btn").click(function() {
    var test_value = $("#file").val();
    var extension = test_value.split('.').pop().toLowerCase();

    if ($.inArray(extension, ['png', 'gif', 'jpeg', 'jpg']) == -1) {
      alert("File ảnh không hợp lệ!");
      return false;
    } else {
      alert("File ảnh hợp lệ!");
      return false;
    }
  });
});*/

$(document).ready(function()
{
        var form = $("#postImg");

        var file = $("#file");
        var mess = $("#mess");
        file.blur(checkFile);

        form.submit(function(){
            if(checkFile()){
               return true;
            }
            else
                return false;
         });

    function checkFile()
    {
        var img = $("#file").val();
        var extension = img.split('.').pop().toLowerCase();
        if($.inArray(extension, ['png', 'gif', 'jpeg', 'jpg']) == -1)
        {
            file.addClass("error");
            mess.text("Vui lòng chọn file ảnh muốn tải lên !");
            mess.addClass("error");
            return false;
        }
        else
        {
            file.removeClass("error");
            mess.text("");
            mess.removeClass("error");
            return true;
        }
    }
});