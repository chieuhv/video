<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>ViVideo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/CheckVideo.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Readmore.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Heart.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Comment.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Follow.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Notification.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Delete.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Search.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Share.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/scrollVideo.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="<%= request.getContextPath()%>/video/home"><i class="fas fa-home"></i> Trang chủ </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-comment"></i> Tin nhắn</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link notification" href="<%= request.getContextPath()%>/video/listnotification" onclick="pressNotification(${ user.getId()})">
                    <i class="fas fa-bell"></i>
                    <span class="bager ${ newnoti=='0'?'noneNoti':'' }" id = "noti">${ newnoti }</span> Thông báo
                    </a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                         <img src="<%= request.getContextPath() %>/${ thumbnail.getThumbnail()}" class="img-circle"> ${user.getName() }
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/video/myaccount"><i class="fas fa-cog"></i> Cài đặt</a>
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/logout"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                      </div>
                  </li>
              </ul>
              <div class="col-sm-6">
                  <form class="input-group" action="search" method="get" id = "formSearch">
                      <input type="text" class="form-control" placeholder="Tìm kiếm bạn bè" name="key" id="key">
                      <div class="input-group-append">
                          <button class="btn btn-success"><i class="fas fa-search"></i></button>
                      </div>
                  </form>
            </div>
              
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <a href="" class="row">
                  <div class="col-sm-12 profileImg text-center"><img src="<%= request.getContextPath() %>/${ profile.getThumbnail()}" class="img-circle"></div> 
                  <div class="col-sm-12 text-center"><h3>${ profile.getName()}</h3></div>
                </a>
                <div class="row">
                    <div class="col-sm-12">
                      <a class="nav-link" href="<%= request.getContextPath() %>/video/follower?id=${ profile.getId()}"><i class="fas fa-user-friends"></i> Người theo dõi</a>
                    </div> 
                    <div class="col-sm-12">
                      <a class="nav-link" href="<%= request.getContextPath()%>/video/following?id=${ profile.getId() }"><i class="fas fa-user-check"></i> Đang theo dõi</a>
                    </div>
                    <div class="col-sm-12 follow${ profile.getId() } m-3" style="display: ${ profile.getFollow()=='1'?'none':'block'}">
                        <button type="button" class="btn btn-info" onclick="profileFollow(${ user.getId()},${ profile.getId() })"><i class="fas fa-plus"></i> Theo dõi</button>
                    </div>
                    <div class="col-sm-12 following${ profile.getId() } m-3" style="display: ${ profile.getFollow()=='0' ||  profile.getId()==user.getId()?'none':'block'}">
                        <button type="button" class="btn btn-success" onclick="profileUnfollow(${ user.getId()},${ profile.getId() })"><i class="fas fa-user-check"></i></button>
                    </div>
                </div>
            </div><!--end col-sm-2-->
            <div class="col-sm-8">
                <div class="col-sm-12 mx-auto p-3">
                    Tạo video mới
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                          <i class="fas fa-plus"></i>
                      </button>
                      <span class="alert ${alert }">
                			${message }
                		</span>
                      <div class="modal" id="myModal">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title">Tải video lên</h4>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                              </div>
                              
                              <!-- Modal body -->
                              <form class="col-sm-12" method="post" action="<%= request.getContextPath() %>/video/postvideo?id=${ user.getId()}" id = "postVideo" enctype="multipart/form-data">
                                  <div class="modal-body">
                                      <div class="col-sm-12">
                                          <input type="file" class="button" name="file" id="file">
                                      </div>
                                      <div id="mess" class=" col-sm-12 text-danger"></div>
                                      <div class="col-sm-12">
                                          <textarea cols="60" rows="6" name="dect" placeholder="Viết gì cho video của bạn..."></textarea>
                                      </div>
                                  </div>
                                  
                                  <!-- Modal footer -->
                                  <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" value="Tải lên"/>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  </div>
                              </form>
                              
                            </div>
                          </div>
                      </div>
                </div><!--end col-sm-12-->
                <div class="col-sm-12 content">
                <c:forEach items="${video }" var="video">
                    <div class="col-sm-12 mx-auto m-3 box">
                        <div class="row d-flex thumbnail p-1">
	                        <a href="<%= request.getContextPath()%>/video/profile?id=${ video.getIdAccount() }"><img src="<%= request.getContextPath() %>/${ video.getThumbnail()}" class="img-circle"></a>
	                        <div class="p-1">
	                            <a href="<%= request.getContextPath()%>/video/profile?id=${ video.getIdAccount() }">${ video.getNameAccount() }</a>
	                            <p class="text">${ video.getPostDate() } lúc ${ video.getPostTime() }</p>
	                        </div>
	                        <div class="dropdown dropright col-sm-1 p-1" style="display:${ user.getId()==video.getIdAccount()?'block':'none'}">
                                <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                  <a href="<%= request.getContextPath()%>/video/detailvideo?idvideo=${ video.getId() }" class="dropdown-item" >Sửa</a>
                                  <a href="#" data-toggle="modal" data-target="#delete" class="dropdown-item delete" data-id="${ video.getId()}" >Xóa bỏ</a>
                                </div>
                            </div>
                        </div>
                        <div class="row p-1">
	                        <div class="read-more">
	                            ${ video.getDect() }
	                        </div>
	                    </div>
                        <div class="video row">
                            <video controls>
                                <source src="<%= request.getContextPath() %>/${ video.getVideo()}" type="video/mp4" />
                            </video>
                        </div>
                        <div class="row d-flex">
                            <div class="d-flex bottom text-center m-2">
                                <div class="bt">
                                	<button type="button" onclick = "pressHeart(${ user.getId()},${ video.getId() },${ video.getIdAccount() })">
                                		<i class="fas fa-heart" id="heart${ video.getId() }" style="color: ${ video.getStatus()=='1'?'red':'gray'}"></i>
                                	</button><span id="heartCount${video.getId() }">${ video.getSumHeart() }</span> 
                                </div>
                                <div class="bt">
	                                <button type="button" onclick="pressComment(${video.getId() },${user.getId() },${ video.getIdAccount() })">
	                                	<div class="textname" id="getname${video.getId() }">${ user.getName() }</div>
	                                	<div class="textname" id="getimg${video.getId() }">${ user.getThumbnail()}</div>
	                                	<div class="textname" id="userid${video.getId() }">${ user.getId()}</div>
	                                	<i class="far fa-comment"></i>
                                	</button><span id="commentCount${video.getId() }">${ video.getSumComment() }</span>
                                </div>
                                <div class="bt"><button type="button" onclick="share(${ video.getId()})"><i class="fas fa-share"></i></button></div>
                            </div>
                        </div>
                        <div id="formComment${ video.getId()}" class="p-1 row comment">
                        	<div id="show${ video.getId()}" class="">
                        	</div>
                            <form method = "get" class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Viết bình luận" id="content${video.getId() }" name="content${video.getId() }">
                                <input type="text" name="userId${ user.getId()}" id="userId${ user.getId()}" value="${ user.getId()}" class="ip"/>
                                <input type="text" name="videoId${video.getId() }" id="videoId${video.getId() }" value="${video.getId() }" class="ip"/>
                                <div class="input-group-append">
                                	<button class="btn btn-primary" type="submit" onclick="postComment(${video.getId() },${user.getId() },${ video.getIdAccount() })" id="btn"><i class="fas fa-paper-plane"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </c:forEach>
                <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  	<div class="modal-dialog" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<h5 class="modal-title" id="exampleModalLabel">Xóa video</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			        		Bạn có chắc chắn muốn xóa không?
			      		</div>
			      		<div class="modal-footer">
				        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
				        	<form method="get" action="<%= request.getContextPath() %>/video/deletevideo">
				        		<input type="text"  name="lastId" id="lastId" value="" class="text-hide"/>
				        		<input class="btn btn-primary" type="submit" value="Xóa bỏ"/>
				        	</form>
			      		</div>
			    	</div>
			  	</div>
				</div>
                </div><!--end col-sm-12-->
            </div><!--end col-sm-8-->
        </div><!--end row-->
    </div>
</body>
</html>