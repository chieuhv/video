<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Tạo tài khoản</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="view/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="view/css/Style.css">
	<link rel="stylesheet" type="text/css" href="view/icon/css/all.css">
	<script type="text/javascript" src="view/js/jquery-3.4.1.js"></script>
	<script type="text/javascript" src="view/js/Account.js"></script>
</head>
<body style="background-image:url(view/images/home.jpg);">
	<div class="container-fluid create">
		<form class="col-sm-5 mx-auto p-5 text-white bg-dark account" id = "createAccount" method="post" action="account">
			<div class="alert ${bg }">
				${message }
			</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Tên</label>
	    		<div class="col-sm-8">
	      			<input type="text" class="form-control input" name="name" id="name" placeholder="Nhập tên của bạn">
	      			<div class="text-danger" id = "mess1"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Mật khẩu</label>
	    		<div class="col-sm-8">
	      			<input type="password" class="form-control input" name="password" id="password" placeholder="Nhập mật khẩu">
	      			<div class="text-danger" id = "mess2"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Ngày sinh</label>
	    		<div class="col-sm-8">
	      			<input type="date" class="form-control input" name="date" id="date" placeholder="Nhập ngày sinh">
	      			<div class="text-danger" id = "mess3"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Giới tính</label>
	    		<div class="col-sm-8">
	      			<input type="radio" class="input" name = "gender" id="male" value="Nam"> Nam
	      			<input type="radio" class="input" name = "gender" id="female" value="Nữ"> Nữ
	      			<div class="text-danger" id = "mess4"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Email</label>
	    		<div class="col-sm-8">
	      			<input type="email" class="form-control input" name="email" id="email" placeholder="Nhập email">
	      			<div class="text-danger" id = "mess5"></div>
	    		</div>
	  		</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label">Số điện thoại</label>
	    		<div class="col-sm-8">
	      			<input type="tel" class="form-control input" name="phone" id="phone" placeholder="Nhập số điên thoại">
	      			<div class="text-danger" id = "mess6"></div>
	    		</div>
	  		</div>
	  
	  		<div class="form-group row">
    			<div class="col-sm-4">
        			
    			</div>
    			<div class="col-sm-8">
        			<button class="btn btn-info" type="submit"><i class="fas fa-plus"></i> Tạo tài khoản</button>
        			<button class="btn btn-primary" id="back" type="button"><i class="fas fa-fast-backward"></i> Quay lại</button>
    			</div>
			</div>
		</form>
	</div>
</body>
</html>