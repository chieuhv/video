<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <title>Thay ảnh đại diện</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Change.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/CheckImg.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Notification.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Search.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="<%= request.getContextPath()%>/video/home"><i class="fas fa-home"></i> Trang chủ </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-comment"></i> Tin nhắn</a>
                  </li>
                 <li class="nav-item">
                    <a class="nav-link notification" href="<%= request.getContextPath()%>/video/listnotification" onclick="pressNotification(${ user.getId()})">
                    <i class="fas fa-bell"></i>
                    <span class="bager ${ newnoti=='0'?'noneNoti':'' }" id = "noti">${ newnoti }</span> Thông báo
                    </a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                         <img src="<%= request.getContextPath() %>/${ thumbnail.getThumbnail()}" class="img-circle"> ${user.getName() }
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/video/myaccount"><i class="fas fa-cog"></i> Cài đặt</a>
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/logout"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                      </div>
                  </li>
              </ul>
              <div class="col-sm-6">
                  <form class="input-group" action="search" method="get" id = "formSearch">
                      <input type="text" class="form-control" placeholder="Tìm kiếm bạn bè" name="key" id="key">
                      <div class="input-group-append">
                          <button class="btn btn-success"><i class="fas fa-search"></i></button>
                      </div>
                  </form>
            </div> 
              
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <a href="" class="row">
                  <div class="col-sm-12 profileImg text-center"><img src="<%= request.getContextPath() %>/${ thumbnail.getThumbnail()}" class="img-circle"/></div> 
                  <div class="col-sm-12 text-center"><h3>${ user.getName()}</h3></div>
                </a>
                <div class="row">
                    <div class="col-sm-12">
                      <a class="nav-link" href="<%= request.getContextPath() %>/video/follower?id=${ user.getId()}"><i class="fas fa-user-friends"></i> Người theo dõi</a>
                    </div> 
                    <div class="col-sm-12">
                      <a class="nav-link" href="<%= request.getContextPath() %>/video/following?id=${ user.getId()}"><i class="fas fa-user-check"></i> Đang theo dõi</a>
                    </div>
                    <div class="col-sm-12">
                      <a class="nav-link " href="<%= request.getContextPath()%>/video/change"><i class="fas fa-image"></i> Thay ảnh đại diện</a>
                    </div> 
                </div>
            </div><!--end col-sm-2-->
            <div class="col-sm-8">
                <div class="col-sm-12 mx-auto p-3">
                    
                </div><!--end col-sm-12-->
                <div class="alert ${alert }">
                	${message }
                </div>
                <div class="col-sm-12 content">
                    <form class="col-sm-6 d-flex" method="post" action="<%= request.getContextPath()%>/video/change?id=${user.getId() }" id = "postImg" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Chọn ảnh</label>
                            <div class="col-sm-4 choose">
                                <input type="file" class="form-control button" name="file" id="file">
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">Tải lên</button>
                            </div>
                        </div>
                    </form>
                    <div id="mess" class="text-danger"></div>
                    <div class="display">
                        <img src="<%= request.getContextPath() %>/${ thumbnail.getThumbnail()}" id = "blankImg">
                    </div>
                </div><!--end col-sm-12-->
            </div>
            <div class="col-sm-2">
            </div>
        </div><!--end row-->
    </div>
</body>
</html>