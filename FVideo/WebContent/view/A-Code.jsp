<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
   <title>ViVideo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/view/js/page.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/view/js/PageList.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Delete.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Search.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row navbar navbar-expand-lg bg-dark navbar-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="<%= request.getContextPath() %>/manager/admin"><i class="fas fa-home"></i> Trang chủ </a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                         <img src="<%= request.getContextPath() %>/${ admin.getThumbnail()}" class="img-circle"> ${ admin.getName() }
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/manager/profileadmin"><i class="fas fa-cog"></i>Hồ sơ</a>
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/adminlogout"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                      </div>
                  </li>
              </ul>
              <div class="col-sm-6">
                  <form class="input-group" action="adminsearch" method="get" id = "formSearch">
                      <input type="text" class="form-control" placeholder="Tìm kiếm người dùng..." name="key" id="key">
                      <div class="input-group-append">
                          <button class="btn btn-success"><i class="fas fa-search"></i></button>
                      </div>
                  </form>
              </div> 
              
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listuser"><i class="fas fa-user-alt"></i> Người dùng</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/toptrending"><i class="fas fa-heart"></i> Xếp hạng video </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listvideo"><i class="fas fa-file-video"></i> Danh sách video</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/newlistvideo"><i class="fas fa-video"></i> Video mới</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listcode"><i class="fas fa-qrcode"></i> Mã xác nhận</a>
                    </li>
                </ul>
            </div><!--end col-sm-2-->
            <div class="col-sm-10 col-right">
            	<h4 class="row text-center p-3 text-success">QUẢN LÝ MÃ CODE</h4>
                <table class="table" id="mytable">
				    <thead class="thead-dark">
				      <tr>
				        <th>Mã xác nhận</th>
				        <th>Tài khoản email</th>
				        <th>Thao tác</th>
				      </tr>
				    </thead>
				    <tbody>
				    <c:forEach items="${ code }" var="code">
				      <tr class="contentPage">
				        <td>${ code.getCode() }</td>
				        <td>${ code.getEmail() }</td>
				        <td> 
					        <a href="#" data-toggle="modal" data-target="#delete" class="delete" data-id="${ code.getCode() }" ><i class="far fa-trash-alt"></i> Xóa</a>
				        </td>
				      </tr>
				    </c:forEach>
				    </tbody>
				</table>
				<ul class="pagination" id="pagination"></ul>
				<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  	<div class="modal-dialog" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<h5 class="modal-title" id="exampleModalLabel">Xóa mã code</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			        		Bạn có chắc chắn muốn xóa không?
			      		</div>
			      		<div class="modal-footer">
				        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
				        	<form method="get" action="<%= request.getContextPath() %>/manager/deletecode">
				        		<input type="text"  name="lastId" id="lastId" value="" class="text-hide"/>
				        		<input class="btn btn-primary" type="submit" value="Xóa bỏ"/>
				        	</form>
			      		</div>
			    	</div>
			  	</div>
				</div>
            </div><!--end col-sm-8-->
        </div><!--end row-->
    </div>
</body>
</html>