<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
   <title>ViVideo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/view/js/page.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/view/js/PageList.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Delete.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Readmore.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Search.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row navbar navbar-expand-lg bg-dark navbar-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="<%= request.getContextPath() %>/manager/admin"><i class="fas fa-home"></i> Trang chủ </a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                         <img src="<%= request.getContextPath() %>/${ admin.getThumbnail()}" class="img-circle"> ${ admin.getName() }
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/manager/profileadmin"><i class="fas fa-cog"></i>Hồ sơ</a>
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/adminlogout"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                      </div>
                  </li>
              </ul>
              <div class="col-sm-6">
                  <form class="input-group" action="adminsearch" method="get" id = "formSearch">
                      <input type="text" class="form-control" placeholder="Tìm kiếm người dùng..." name="key" id="key">
                      <div class="input-group-append">
                          <button class="btn btn-success"><i class="fas fa-search"></i></button>
                      </div>
                  </form>
              </div> 
              
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listuser"><i class="fas fa-user-alt"></i> Người dùng</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/toptrending"><i class="fas fa-heart"></i> Xếp hạng video </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listvideo"><i class="fas fa-file-video"></i> Danh sách video</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/newlistvideo"><i class="fas fa-video"></i> Video mới</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listcode"><i class="fas fa-qrcode"></i> Mã xác nhận</a>
                    </li>
                </ul>
            </div><!--end col-sm-2-->
            <div class="col-sm-10 col-right">
            	<form class="col-sm-12 mx-auto m-3 p-3 box" method="post" action="">
            		<a href="" class="row">
	                  <div class="col-sm-12 profileImg text-center"><img src="<%= request.getContextPath() %>/${ account.getThumbnail()}" class="img-circle"></div> 
	                  <div class="col-sm-12 text-center"><h3>${ account.getName()}</h3></div>
	                </a>
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-4 col-form-label">Tên</label>
                          <div class="col-sm-8">
                              <input type="text" class="form-control input" name="name" id="name" placeholder="Nhập tên của bạn" value="${ account.getName() }">
                              <div class="text-danger" id = "mess1"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-4 col-form-label">Ngày sinh</label>
                          <div class="col-sm-8">
                              <input type="date" class="form-control input" name="date" id="date" placeholder="Nhập ngày sinh" value="${ account.getBirthDate() }">
                              <div class="text-danger" id = "mess3"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-4 col-form-label">Giới tính</label>
                          <div class="col-sm-8">
                              <input type="radio" class="input" name = "gender" id="male" value="Nam" ${account.getGender()=='Nam'?'checked':'' }> Nam
                              <input type="radio" class="input" name = "gender" id="female" value="Nữ" ${account.getGender()=='Nữ'?'checked':'' }> Nữ
                              <div class="text-danger" id = "mess4"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-4 col-form-label">Email</label>
                          <div class="col-sm-8">
                              <input type="email" class="form-control input" name="email" id="email" placeholder="Nhập email" value="${ account.getEmail() }">
                              <div class="text-danger" id = "mess5"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="inputEmail3" class="col-sm-4 col-form-label">Số điện thoại</label>
                          <div class="col-sm-8">
                              <input type="tel" class="form-control input" name="phone" id="phone" placeholder="Nhập số điên thoại" value="${ account.getPhone() }">
                              <div class="text-danger" id = "mess6"></div>
                          </div>
                      </div>
                      <!--  <div class="form-group row">
                          <div class="col-sm-4">
                                      
                          </div>
                          <div class="col-sm-8">
                              <button class="btn btn-info" type="submit"><i class="fas fa-edit"></i> Sửa tài khoản</button>
                              <button class="btn btn-primary" id="back" type="button"><i class="fas fa-fast-backward"></i> Quay lại</button>
                          </div>
                       </div>-->
                        <div class="row text-center">
                        </div>
                    </form>
				</div>
            </div><!--end col-sm-10-->
        </div><!--end row-->
    </div>
</body>
</html>