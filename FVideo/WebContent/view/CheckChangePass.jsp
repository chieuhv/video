<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Mã xác nhận</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset = "utf-8">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
	<script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/view/js/CreateCode.js"></script>
</head>
<body style="background-image:url(view/images/home.jpg);">
	<div class="container-fluid login">
		<form class="col-sm-5 mx-auto p-5 text-white bg-dark register" id = "createCode" method="post" action="<%= request.getContextPath() %>/video/updatepass">
			<div class="row text-center">
				<h5 class="col-sm-12 m-3">Nhập mã xác nhận để chắc chắn đó là bạn</h5>
			</div>
			<div class="alert ${alert }">
				${message }
			</div>
	  		<div class="form-group row">
	    		<label for="inputEmail3" class="col-sm-4 col-form-label"> Mã xác nhận</label>
	    		<div class="col-sm-8">
	      			<input type="text" class="form-control input" name="code" id="code" placeholder="Nhập mã xác nhận">
	      			<div class="text-danger" id = "mess"></div>
	    		</div>
	  		</div>	
	  		<div class="form-group row">
    			<div class="col-sm-4">
        			
    			</div>
    			<div class="col-sm-8">
        			<button class="btn btn-info" type="submit"><i class="fas fa-sign-in-alt"></i> Gửi mã</button>
        			<button class="btn btn-primary" id="back" type="button"><i class="fas fa-fast-backward"></i> Quay lại</button>
    			</div>
			</div>
		</form>
	</div>
</body>
</html>