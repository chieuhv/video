<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
   <title>ViVideo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/icon/css/all.css">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/view/css/Style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/view/js/Search.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row navbar navbar-expand-lg bg-dark navbar-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="<%= request.getContextPath() %>/manager/admin"><i class="fas fa-home"></i> Trang chủ </a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                         <img src="<%= request.getContextPath() %>/${ admin.getThumbnail()}" class="img-circle"> ${ admin.getName() }
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/manager/profileadmin"><i class="fas fa-cog"></i>Hồ sơ</a>
                        <a class="dropdown-item" href="<%= request.getContextPath() %>/adminlogout"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                      </div>
                  </li>
              </ul>
              <div class="col-sm-6">
                  <form class="input-group" action="adminsearch" method="get" id = "formSearch">
                      <input type="text" class="form-control" placeholder="Tìm kiếm người dùng..." name="key" id="key">
                      <div class="input-group-append">
                          <button class="btn btn-success"><i class="fas fa-search"></i></button>
                      </div>
                  </form>
              </div> 
              
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listuser"><i class="fas fa-user-alt"></i> Người dùng</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/toptrending"><i class="fas fa-heart"></i> Xếp hạng video </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listvideo"><i class="fas fa-file-video"></i> Danh sách video</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/newlistvideo"><i class="fas fa-video"></i> Video mới</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<%= request.getContextPath() %>/manager/listcode"><i class="fas fa-qrcode"></i> Mã xác nhận</a>
                    </li>
                </ul>
            </div><!--end col-sm-2-->
            <div class="col-sm-10 col-right">
                <div class="row m-2">
                    <div class="col-sm-3 bg-primary text-center acol">
                      Tổng số người dùng: ${ user }
                    </div>
                    <div class="col-sm-3 bg-success text-center acol">
                      Số người đang online: ${ counter }
                    </div>
                    <div class="col-sm-3 bg-info text-center acol">
                      Tổng số video: ${ video }
                    </div>
                    <div class="col-sm-3 bg-secondary text-center acol">
                      Video mới: ${ newvideo }
                    </div>
                </div>
                <div class="row m-2">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3993006898763!2d105.7813963143781!3d21.016703193567036!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454ab43c0c4db%3A0xdb6effebd6991106!2sKeangnam%20Hanoi%20Landmark%20Tower!5e0!3m2!1svi!2s!4v1591257218283!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div><!--end col-sm-8-->
        </div><!--end row-->
    </div>
</body>
</html>