package comment.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CommentDAO;
import dao.NotificationDAO;
import model.Comment;
import model.Notification;

@WebServlet("/video/addcomment")
public class CommentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public CommentController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date date = new Date();
		String time = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(time);
		String datetime = sdf.format(date);
		
		LocalTime lct = LocalTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		String hour = lct.format(dtf);
		
		CommentDAO commentDao = new CommentDAO();
		NotificationDAO notificationDao = new NotificationDAO();
		
		String idAccount = request.getParameter("account");
		String idVideo = request.getParameter("video");
		String content = request.getParameter("content");
		String account = request.getParameter("idaccount");
		
		commentDao.addComment(new Comment(idAccount,idVideo,datetime,hour,content));
		
		String comment = "Đã bình luận video của bạn";
		if(!idAccount.equals(account))
		{
			notificationDao.addNotification(new Notification(idAccount,comment,datetime,hour,"0",idVideo,account));
		}
	}

}
