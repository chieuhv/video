package comment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.CommentDAO;
import model.Comment;


@WebServlet("/video/listcomment")
public class ListComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public ListComment() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
		CommentDAO commentDao = new CommentDAO();
		PrintWriter out = response.getWriter();
		String idVideo = request.getParameter("id");
		List<Comment> listComment = commentDao.listComment(idVideo);
		Gson gson = new Gson();
        String objectToReturn = gson.toJson(listComment); 
        out.write(objectToReturn); 
        out.flush();
	}
}
