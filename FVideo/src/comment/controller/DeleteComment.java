package comment.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CommentDAO;


@WebServlet("/video/deletecomment")
public class DeleteComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public DeleteComment() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommentDAO commentDao = new CommentDAO();
		String id = request.getParameter("id");
		commentDao.deleteComment(id);
	}

	

}
