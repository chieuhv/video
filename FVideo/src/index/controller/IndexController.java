package index.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import dao.VideoDAO;
import model.Account;
import model.Login;
import model.Video;
import utils.Message;

@WebServlet("/video/home")
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public IndexController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.VIDEO);
			request.setAttribute("alert", Message.BG);
		}
		VideoDAO videoDao = new VideoDAO();
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		String id = login.getId();
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		List<Video> listVideo = videoDao.listVideo(id);
		request.setAttribute("video", listVideo);
		RequestDispatcher de = request.getRequestDispatcher("/view/Index.jsp");
		de.forward(request, response);
	}
}
