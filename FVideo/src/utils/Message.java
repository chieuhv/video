package utils;

public interface Message {
	
	String BG = "alert-danger";
	
	String SUCCESS = "alert-success";
	
	String CODEFAILDE = "Mã xác nhận không đúng !";
	
	String LOGIN = "Tài khoản đăng nhập không chính xác";
	
	String UPLOAD = "Tải ảnh không thành công !";
	
	String VIDEO = "Tải video không thành công";
	
	String UPDATEACCOUNT = "Tài khoản email bạn vừa nhập đã tồn tại";
	
	String CHECKPASS = "Kiểm tra lại email và mật khẩu cũ của bạn";
	
	String CHECKMAIL = "Tài khoản email bạn vừa nhập không tồn tại";
}
