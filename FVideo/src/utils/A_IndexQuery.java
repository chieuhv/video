package utils;

public interface A_IndexQuery {
	String TOTALUSER = "SELECT ID_ACCOUNT,NAME,PASSWORDS,BIRTHDATE,GENDER,EMAIL,PHONE,THUMBNAIL,NOTE FROM ACCOUNT WHERE NOTE=?";
	
	String DETAILUSER = "SELECT ID_ACCOUNT,NAME,PASSWORDS,BIRTHDATE,GENDER,EMAIL,PHONE,THUMBNAIL,NOTE FROM ACCOUNT WHERE ID_ACCOUNT=?";
	
	String SEARCHUSER = "SELECT ID_ACCOUNT,NAME,PASSWORDS,BIRTHDATE,GENDER,EMAIL,PHONE,THUMBNAIL,NOTE FROM ACCOUNT WHERE NOTE=? AND NAME LIKE ?";
	
	String LISTVIDEO = "SELECT ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME "
			+ "FROM VIDEO INNER JOIN ACCOUNT ON VIDEO.ID_ACCOUNT=ACCOUNT.ID_ACCOUNT ORDER BY POSTDATE DESC,POSTTIME DESC";
	
	String DETAILVIDEO = "SELECT ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME "
			+ "FROM VIDEO INNER JOIN ACCOUNT ON VIDEO.ID_ACCOUNT=ACCOUNT.ID_ACCOUNT WHERE ID_VIDEO=? ORDER BY POSTDATE DESC,POSTTIME DESC";
	
	String NEWVIDEO = "SELECT ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME "
			+ "FROM VIDEO INNER JOIN ACCOUNT ON VIDEO.ID_ACCOUNT=ACCOUNT.ID_ACCOUNT WHERE POSTDATE = ? ORDER BY POSTDATE DESC,POSTTIME DESC";
	
	String VIDEOUSER = "SELECT ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME "
			+ "FROM VIDEO INNER JOIN ACCOUNT ON VIDEO.ID_ACCOUNT=ACCOUNT.ID_ACCOUNT WHERE ACCOUNT.ID_ACCOUNT=? ORDER BY POSTDATE DESC,POSTTIME DESC";
	
	String DLETEVIDEO = "DELETE FROM VIDEO WHERE ID_VIDEO=?";
	
	String DELETEUSER = "DELETE FROM ACCOUNT WHERE ID_ACCOUNT=?";
	
	String DELETEHEART = "DELETE FROM HEART WHERE ID_ACCOUNT=?";
	
	String DELETECOMMENT = "DELETE FROM COMMENT WHERE ID_ACCOUNT=?";
	
	String DELETEFOLLOWER = "DELETE FROM FOLLOW WHERE FOLLOWERID=?";
	
	String DELETEFOLLOWING = "DELETE FROM FOLLOW WHERE FOLLOWINGID=?";
	
	String CODE = "SELECT * FROM CREATECODE";
	
	String DELETECODE = "DELETE FROM CREATECODE WHERE CODE=?";
	
	String TOPTRENDING = "SELECT VIDEO.ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME, COUNT(HEART.ID_ACCOUNT)"
			+"FROM ((VIDEO INNER JOIN ACCOUNT ON VIDEO.ID_ACCOUNT=ACCOUNT.ID_ACCOUNT)INNER JOIN HEART ON VIDEO.ID_VIDEO=HEART.ID_VIDEO)"
			+"GROUP BY VIDEO.ID_VIDEO,DECT,POSTDATE,POSTTIME,VIDEO,THUMBNAIL,VIDEO.ID_ACCOUNT,NAME ORDER BY COUNT(HEART.ID_ACCOUNT) DESC";
}
