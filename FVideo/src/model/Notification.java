package model;

public class Notification {
	private String notificationId;
	private String notificationer;
	private String content;
	private String date;
	private String time;
	private String status;
	private String idVideo;
	private String idAccount;
	
	private String thumbnail;
	private String accountName;
	
	public Notification(String notificationer, String content, String date, String time, String status, String idVideo,
			String idAccount) {
		super();
		this.notificationer = notificationer;
		this.content = content;
		this.date = date;
		this.time = time;
		this.status = status;
		this.idVideo = idVideo;
		this.idAccount = idAccount;
	}

	public Notification(String notificationId, String notificationer, String content, String date, String time,
			String status, String idVideo, String idAccount) {
		super();
		this.notificationId = notificationId;
		this.notificationer = notificationer;
		this.content = content;
		this.date = date;
		this.time = time;
		this.status = status;
		this.idVideo = idVideo;
		this.idAccount = idAccount;
	}
	
	public Notification(String notificationId, String notificationer, String content, String date, String time,
			String status, String idVideo, String idAccount, String thumbnail, String accountName) {
		super();
		this.notificationId = notificationId;
		this.notificationer = notificationer;
		this.content = content;
		this.date = date;
		this.time = time;
		this.status = status;
		this.idVideo = idVideo;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.accountName = accountName;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationer() {
		return notificationer;
	}

	public void setNotificationer(String notificationer) {
		this.notificationer = notificationer;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdVideo() {
		return idVideo;
	}

	public void setIdVideo(String idVideo) {
		this.idVideo = idVideo;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
}
