package model;

public class Follow {
	private String followId;
	private String followerId;
	private String followingId;
	private String accountId;
	
	private String accountName;
	private String thumbnail;
	private String status;
	
	public Follow(String followId, String followerId, String followingId, String accountId) {
		super();
		this.followId = followId;
		this.followerId = followerId;
		this.followingId = followingId;
		this.accountId = accountId;
	}
	
	
	public Follow(String followerId, String followingId, String accountId) {
		super();
		this.followerId = followerId;
		this.followingId = followingId;
		this.accountId = accountId;
	}

	
	public Follow(String followId, String followerId, String followingId, String accountId, String accountName,
			String thumbnail) {
		super();
		this.followId = followId;
		this.followerId = followerId;
		this.followingId = followingId;
		this.accountId = accountId;
		this.accountName = accountName;
		this.thumbnail = thumbnail;
	}

	
	public Follow(String followId, String followerId, String followingId, String accountId, String accountName,
			String thumbnail, String status) {
		super();
		this.followId = followId;
		this.followerId = followerId;
		this.followingId = followingId;
		this.accountId = accountId;
		this.accountName = accountName;
		this.thumbnail = thumbnail;
		this.status = status;
	}


	public String getFollowId() {
		return followId;
	}


	public void setFollowId(String followId) {
		this.followId = followId;
	}


	public String getFollowerId() {
		return followerId;
	}


	public void setFollowerId(String followerId) {
		this.followerId = followerId;
	}


	public String getFollowingId() {
		return followingId;
	}


	public void setFollowingId(String followingId) {
		this.followingId = followingId;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getThumbnail() {
		return thumbnail;
	}


	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
