package model;

public class Code {
	private String code;
	private String email;
	
	public Code(String code, String email) {
		super();
		this.code = code;
		this.email = email;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
}
