package model;

public class Image {
	private String img;

	public Image(String img) {
		super();
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	
}
