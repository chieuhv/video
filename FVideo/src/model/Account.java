package model;

public class Account {
	private String id;
	private String name;
	private String password;
	private String birthDate;
	private String gender;
	private String email;
	private String phone;
	private String thumbnail;
	private String note;
	
	private String follow;
	private String totalFollower;
	private String totalFollowing;

	public Account(String email) {
		super();
		this.email = email;
	}
	
	public Account(String password, String note) {
		super();
		this.password = password;
		this.note = note;
	}
	
	public Account(String password, String email, String note) {
		super();
		this.password = password;
		this.email = email;
		this.note = note;
	}

	public Account(String name, String password, String birthDate, String gender, String email, String phone,
			String thumbnail, String note) {
		super();
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.thumbnail = thumbnail;
		this.note = note;
	}
	
	public Account(String id, String name, String password, String birthDate, String gender, String email, String phone,
			String thumbnail, String note) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.thumbnail = thumbnail;
		this.note = note;
	}
	
	public Account(String id, String name, String password, String birthDate, String gender, String email, String phone,
			String thumbnail, String note, String totalFollower, String totalFollowing) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.thumbnail = thumbnail;
		this.note = note;
		this.totalFollower = totalFollower;
		this.totalFollowing = totalFollowing;
	}

	public Account(String id, String name, String password, String birthDate, String gender, String email, String phone,
			String thumbnail, String note, String follow, String totalFollower, String totalFollowing) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.thumbnail = thumbnail;
		this.note = note;
		this.follow = follow;
		this.totalFollower = totalFollower;
		this.totalFollowing = totalFollowing;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getFollow() {
		return follow;
	}

	public void setFollow(String follow) {
		this.follow = follow;
	}

	public String getTotalFollower() {
		return totalFollower;
	}

	public void setTotalFollower(String totalFollower) {
		this.totalFollower = totalFollower;
	}

	public String getTotalFollowing() {
		return totalFollowing;
	}

	public void setTotalFollowing(String totalFollowing) {
		this.totalFollowing = totalFollowing;
	}
	
}
