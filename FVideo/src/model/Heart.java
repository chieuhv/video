package model;

public class Heart {
	private String idAccount;
	private String idVideo;
	
	public Heart(String idAccount, String idVideo) {
		super();
		this.idAccount = idAccount;
		this.idVideo = idVideo;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getIdVideo() {
		return idVideo;
	}

	public void setIdVideo(String idVideo) {
		this.idVideo = idVideo;
	}
	
}
