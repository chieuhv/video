package model;

public class Video {
	private String id;
	private String dect;
	private String postDate;
	private String postTime;
	private String video;
	private String idAccount;
	
	private String thumbnail;
	private String nameAccount;
	private String sumHeart;
	private String sumComment;
	private String status;
	private String follow;
	private int top;
	
	public Video(String video) {
		super();
		this.video = video;
	}

	public Video(String dect, String postDate, String postTime) {
		super();
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
	}

	public Video(String dect, String postDate, String postTime, String video, String idAccount) {
		super();
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
	}

	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
	}

	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount,
			String thumbnail, String nameAccount) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.nameAccount = nameAccount;
	}

	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount,
			String thumbnail, String nameAccount, String sumHeart, String sumComment, String status, String follow) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.nameAccount = nameAccount;
		this.sumHeart = sumHeart;
		this.sumComment = sumComment;
		this.status = status;
		this.follow = follow;
	}
	
	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount,
			String thumbnail, String nameAccount, String sumHeart, String sumComment, String status) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.nameAccount = nameAccount;
		this.sumHeart = sumHeart;
		this.sumComment = sumComment;
		this.status = status;
	}
	
	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount,
			String thumbnail, String nameAccount, String sumHeart, String sumComment) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.nameAccount = nameAccount;
		this.sumHeart = sumHeart;
		this.sumComment = sumComment;
	}
	
	public Video(String id, String dect, String postDate, String postTime, String video, String idAccount,
			String thumbnail, String nameAccount, String sumHeart, String sumComment, int top) {
		super();
		this.id = id;
		this.dect = dect;
		this.postDate = postDate;
		this.postTime = postTime;
		this.video = video;
		this.idAccount = idAccount;
		this.thumbnail = thumbnail;
		this.nameAccount = nameAccount;
		this.sumHeart = sumHeart;
		this.sumComment = sumComment;
		this.top = top;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDect() {
		return dect;
	}

	public void setDect(String dect) {
		this.dect = dect;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getPostTime() {
		return postTime;
	}

	public void setPostTime(String postTime) {
		this.postTime = postTime;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getNameAccount() {
		return nameAccount;
	}

	public void setNameAccount(String nameAccount) {
		this.nameAccount = nameAccount;
	}

	public String getSumHeart() {
		return sumHeart;
	}

	public void setSumHeart(String sumHeart) {
		this.sumHeart = sumHeart;
	}

	public String getSumComment() {
		return sumComment;
	}

	public void setSumComment(String sumComment) {
		this.sumComment = sumComment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFollow() {
		return follow;
	}

	public void setFollow(String follow) {
		this.follow = follow;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

}
