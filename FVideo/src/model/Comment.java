package model;

public class Comment {
	private String id;
	private String idAccount;
	private String idVideo;
	private String date;
	private String time;
	private String content;
	
	private String accountName;
	private String thumbnail;
	
	public Comment(String idAccount, String idVideo, String date, String time, String content) {
		super();
		this.idAccount = idAccount;
		this.idVideo = idVideo;
		this.date = date;
		this.time = time;
		this.content = content;
	}

	public Comment(String id, String idAccount, String idVideo, String date, String time, String content,
			String accountName, String thumbnail) {
		super();
		this.id = id;
		this.idAccount = idAccount;
		this.idVideo = idVideo;
		this.date = date;
		this.time = time;
		this.content = content;
		this.accountName = accountName;
		this.thumbnail = thumbnail;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getIdVideo() {
		return idVideo;
	}

	public void setIdVideo(String idVideo) {
		this.idVideo = idVideo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
}
