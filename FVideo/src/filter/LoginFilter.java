package filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Login;

@WebFilter({"/video/*","/manager/*"})
public class LoginFilter implements Filter {

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		HttpSession sessionUser = req.getSession();
		Login loginUser = (Login) sessionUser.getAttribute("user");
		
		HttpSession sessionAdmin = req.getSession();
		Login loginAdmin = (Login) sessionAdmin.getAttribute("admin");
		
		String urlPattern = req.getRequestURI();
		String servletPath = req.getServletPath();
		
		if(loginAdmin==null && !urlPattern.endsWith("adminlogin") && !urlPattern.endsWith("A-Login.jsp") && servletPath.startsWith("/manager"))
		{
			res.sendRedirect(req.getContextPath()+"/adminlogin");
		}
		else
		if(loginUser==null && !urlPattern.endsWith("login") && !urlPattern.endsWith("Login.jsp") && servletPath.startsWith("/video"))
		{
			res.sendRedirect(req.getContextPath()+"/login");
		}
		else
		{
			try {
				chain.doFilter(request, response);
			}
			catch(Exception e)
			{
				if(servletPath.startsWith("/manager"))
				{
					res.sendRedirect(req.getContextPath()+"/adminlogin");
				}
				if(servletPath.startsWith("/video"))
				{
					res.sendRedirect(req.getContextPath()+"/login");
				}
			}
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
