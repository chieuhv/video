package login.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.LoginDAO;
import model.Code;
import model.Login;
import model.SendCode;
import utils.Message;

@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.LOGIN);
			request.setAttribute("alert", Message.BG);
		}
		RequestDispatcher red = request.getRequestDispatcher("/view/Login.jsp");
		red.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDAO loginDao = new LoginDAO();
		AccountDAO accountDao = new AccountDAO();
		
		String email = request.getParameter("email");
		String pass = request.getParameter("password");
		
		if(loginDao.checkUser(email, pass))
		{
			HttpSession sessionLogin = request.getSession();
			Login login = new Login(pass,email);
			sessionLogin.setAttribute("login", login);
			
			String createCode = SendCode.randomCode();
			accountDao.creatCode(new Code(createCode, email));
			SendCode.senMail(createCode, email);
			RequestDispatcher red = request.getRequestDispatcher("/view/CheckLogin.jsp");
			red.forward(request, response);
		}
		else
		{
			response.sendRedirect(request.getContextPath() +"/login?message=loginfailed&alert=bg");
		}
	}

}
