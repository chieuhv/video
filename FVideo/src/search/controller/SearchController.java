package search.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import dao.SearchDAO;
import model.Account;
import model.Login;
import model.Search;

@WebServlet("/video/search")
public class SearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public SearchController() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SearchDAO searchDao = new SearchDAO();
		AccountDAO accountDao = new AccountDAO();
		
		String key = request.getParameter("key");
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		List<Search> search = searchDao.resultSearch(key, "0", login.getId());
		request.setAttribute("search", search);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/Search.jsp");
		de.forward(request, response);
	}
}
