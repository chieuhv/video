package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Heart;
import utils.HeartQuery;

public class HeartDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public boolean checkHeart(String idAccount,String idVideo)
	{
		Heart heart = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(HeartQuery.CHECKHEART);
			preparedStatement.setString(1,idAccount);
			preparedStatement.setString(2,idVideo);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String account = result.getString(1);
				String video = result.getString(2);
				heart = new Heart(account,video);
			}
			if(heart!=null)
			{
				return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public void addHeart(Heart heart)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(HeartQuery.INSERT);
			preparedStatement.setString(1, heart.getIdAccount());
			preparedStatement.setString(2, heart.getIdVideo());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	public void deleteHeart(String idAccount, String idVideo)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(HeartQuery.DELETE);
			preparedStatement.setString(1, idAccount);
			preparedStatement.setString(2, idVideo);
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	public List<Heart> listHeart(String idVideo)
	{
		List<Heart> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(HeartQuery.LISTHEART);
			preparedStatement.setString(1, idVideo);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String account = result.getString(1);
				String video = result.getString(2);
				list.add(new Heart(account,video));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String countHeart(String idVideo)
	{
		HeartDAO heartDao = new HeartDAO();
		int count = heartDao.listHeart(idVideo).size();
		String heart = String.valueOf(count);
		return heart;
	}
}
