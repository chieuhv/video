package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Follow;
import utils.FollowQuery;

public class FollowDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public void addFollow(Follow follow)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.ADD);
			preparedStatement.setString(1, follow.getFollowerId());
			preparedStatement.setString(2, follow.getFollowingId());
			preparedStatement.setString(3, follow.getAccountId());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public boolean checkFollow(String followerId, String followingId)
	{
		Follow follow = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.CHECK);
			preparedStatement.setString(1, followerId);
			preparedStatement.setString(2, followingId);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String followId = result.getString(1);
				String follower = result.getString(2);
				String following= result.getString(3);
				String accountId = result.getString(4);
				follow = new Follow(followId,follower,following,accountId);
			}
			if(follow!=null)
			{
				return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(result!=null)
				{
					result.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public List<Follow> listFollowing(String id,String userid)
	{
		List<Follow> list = new ArrayList<>();
		List<Follow> listFollow = new ArrayList<>();
		FollowDAO followDao = new FollowDAO();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.FOLLOWING);
			preparedStatement.setString(1, id);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String followId = result.getString(1);
				String follower = result.getString(2);
				String following= result.getString(3);
				String accountId = result.getString(4);
				String accountName = result.getString(5);
				String thumbnail = result.getString(6);
				
				list.add(new Follow(followId,follower,following,accountId,accountName,thumbnail));
			}
			String status = "0";
			for(Follow follow : list)
			{
				if(followDao.checkFollow(userid, follow.getFollowingId())){
					status="1";
				}
				listFollow.add(new Follow(follow.getFollowId(),follow.getFollowerId(),follow.getFollowingId(),follow.getAccountId(),
						follow.getAccountName(),follow.getThumbnail(),status));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			if(result!=null)
			{
				result.close();
			}
			if(preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if(connection!=null)
			{
				connection.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return listFollow;
	}
	
	public List<Follow> listFollower(String id,String userid)
	{
		List<Follow> list = new ArrayList<>();
		List<Follow> listFollow = new ArrayList<>();
		FollowDAO followDao = new FollowDAO();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.FOLLOWER);
			preparedStatement.setString(1, id);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String followId = result.getString(1);
				String follower = result.getString(2);
				String following= result.getString(3);
				String accountId = result.getString(4);
				String accountName = result.getString(5);
				String thumbnail = result.getString(6);
				
				list.add(new Follow(followId,follower,following,accountId,accountName,thumbnail));
			}
			String status = "0";
			for(Follow follow : list)
			{
				if(followDao.checkFollow(userid, follow.getFollowerId())){
					status="1";
				}
				listFollow.add(new Follow(follow.getFollowId(),follow.getFollowerId(),follow.getFollowingId(),follow.getAccountId(),
						follow.getAccountName(),follow.getThumbnail(),status));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			if(result!=null)
			{
				result.close();
			}
			if(preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if(connection!=null)
			{
				connection.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return listFollow;
	}
	
	public void deleteFollow(String followerId, String followingId)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.DELETE);
			preparedStatement.setString(1, followerId);
			preparedStatement.setString(2, followingId);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public String totalFollower(String id,String userid)
	{
		FollowDAO followDao = new FollowDAO();
		int count = followDao.listFollower(id, userid).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public String totalFollowing(String id,String userid)
	{
		FollowDAO followDao = new FollowDAO();
		int count = followDao.listFollowing(id, userid).size();
		String total = String.valueOf(count);
		return total;
	}
}
