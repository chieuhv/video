package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connection.DBConnection;
import model.Code;
import model.Login;
import utils.LoginQuery;

public class LoginDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public boolean checkAdmin(String mail, String pass)
	{
		Login login = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(LoginQuery.CHECK);
			preparedStatement.setString(1, mail);
			preparedStatement.setString(2, pass);
			
			result = preparedStatement.executeQuery();
			String note = "";
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				note = result.getString(9); 
				
				login = new Login(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
			
			if(login!=null)
			{
				if(login.getEmail().equals(mail) && login.getPassword().equals(pass) && note.equals("1"))
				{
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean checkUser(String mail, String pass)
	{
		Login login = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(LoginQuery.CHECK);
			preparedStatement.setString(1, mail);
			preparedStatement.setString(2, pass);
			
			result = preparedStatement.executeQuery();
			String note = "";
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				note = result.getString(9); 
				
				login = new Login(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
			
			if(login!=null)
			{
				if(login.getEmail().equals(mail) && login.getPassword().equals(pass) && note.equals("0"))
				{
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public Login getValue(String mail, String pass)
	{
		Login login = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(LoginQuery.CHECK);
			preparedStatement.setString(1, mail);
			preparedStatement.setString(2, pass);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				String note = result.getString(9); 
				
				login = new Login(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return login;
	}
	
	public Code getCode(String code)
	{
		Code bitCode = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(LoginQuery.GETCODE);
			preparedStatement.setString(1, code);
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String codeValue = result.getString(1);
				String emailValue = result.getString(2);
				
				bitCode = new Code(codeValue,emailValue);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return bitCode;
	}
	
	public void deleteCode(String code)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(LoginQuery.DELETECODE);
			preparedStatement.setString(1, code);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
