package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Video;
import utils.VideoQuery;

public class VideoDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public void addVideo(Video video)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.INSERT);
			if(video.getDect()!=null)
			{
				video.setDect(new String(video.getDect().getBytes("iso-8859-1"),"UTF-8"));
			}
			video.setVideo(new String(video.getVideo().getBytes("iso-8859-1"),"UTF-8"));
			preparedStatement.setString(1, video.getDect());
			preparedStatement.setString(2, video.getPostDate());
			preparedStatement.setString(3, video.getPostTime());
			preparedStatement.setString(4, video.getVideo());
			preparedStatement.setString(5, video.getIdAccount());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public List<Video> listVideo(String idUser)
	{
		List<Video> listVd = new ArrayList<>();
		List<Video> list = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		FollowDAO followDao = new FollowDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.LISTVIDEO);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				String status = "0";
				if(heartDao.checkHeart(idUser, itemVd.getId()))
				{
					status = "1";
				}
				String follow = "0";
				if(followDao.checkFollow(idUser, itemVd.getIdAccount()) || idUser.equals(itemVd.getIdAccount()))
				{
					follow = "1";
				}
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment,status, follow));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<Video> videoOfProfile(String idUser, String accountId)
	{
		List<Video> listVd = new ArrayList<>();
		List<Video> list = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.VIDEOPROFILE);
			preparedStatement.setString(1, accountId);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				String status = "0";
				if(heartDao.checkHeart(idUser, itemVd.getId()))
				{
					status = "1";
				}
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment,status));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public Video detailVideo(String idVideo, String idAccount)
	{
		Video videoDetail = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.DETAIL);
			preparedStatement.setString(1, idVideo);
			preparedStatement.setString(2, idAccount);
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String account = result.getString(6);
				
				videoDetail = new Video(id,dect,postDate,postTime,video,account);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return videoDetail;
	}
	
	public void updateVideo(Video video,String idVideo, String idAccount)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.UPDATE);
			if(video.getDect()!=null)
			{
				video.setDect(new String(video.getDect().getBytes("iso-8859-1"),"UTF-8"));
			}
			preparedStatement.setString(1, video.getDect());
			preparedStatement.setString(2, video.getPostDate());
			preparedStatement.setString(3, video.getPostTime());
			preparedStatement.setString(4, idVideo);
			preparedStatement.setString(5, idAccount);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteVideo(String idVideo, String idAccount)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.DELETE);
			
			preparedStatement.setString(1, idVideo);
			preparedStatement.setString(2, idAccount);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public Video videoOfNoti(String accountId,String idVideo)
	{
		Video videoBefore = null;
		Video videoLast = null;
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.VIDEONOTI);
			preparedStatement.setString(1, accountId);
			preparedStatement.setString(2, idVideo);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				videoBefore = new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount);
			}
			
				String totalHeart = heartDao.countHeart(videoBefore.getId());
				String totalComment = commentDao.countComment(videoBefore.getId());
				String status = "0";
				if(heartDao.checkHeart(accountId, videoBefore.getId()))
				{
					status = "1";
				}
				videoLast = new Video(videoBefore.getId(),videoBefore.getDect(),videoBefore.getPostDate(),videoBefore.getPostTime(),videoBefore.getVideo(),videoBefore.getIdAccount(),
						videoBefore.getThumbnail(),videoBefore.getNameAccount(),totalHeart,totalComment,status);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return videoLast;
	}
	
	public Video shareVideo(String id)
	{
		Video share = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(VideoQuery.SHAREVIDEO);
			preparedStatement.setString(1, id);
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				
				String video = result.getString(1);
				
				share= new Video(video);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return share;
	}
}
