package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Account;
import model.Code;
import model.Follow;
import model.Video;
import utils.A_IndexQuery;
import utils.FollowQuery;

public class AdminDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public List<Account> listUser()
	{
		List<Account> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.TOTALUSER);
			preparedStatement.setString(1,"0");
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3);
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6);
				String phone = result.getString(7);
				String thumbnail = result.getString(8);
				String note = result.getString(9);
				list.add(new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<Follow> listFollower(String id)
	{
		List<Follow> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.FOLLOWER);
			preparedStatement.setString(1, id);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String followId = result.getString(1);
				String follower = result.getString(2);
				String following= result.getString(3);
				String accountId = result.getString(4);
				String accountName = result.getString(5);
				String thumbnail = result.getString(6);
				
				list.add(new Follow(followId,follower,following,accountId,accountName,thumbnail));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			if(result!=null)
			{
				result.close();
			}
			if(preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if(connection!=null)
			{
				connection.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	public List<Follow> listFollowing(String id)
	{
		List<Follow> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FollowQuery.FOLLOWING);
			preparedStatement.setString(1, id);
			
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String followId = result.getString(1);
				String follower = result.getString(2);
				String following= result.getString(3);
				String accountId = result.getString(4);
				String accountName = result.getString(5);
				String thumbnail = result.getString(6);
				
				list.add(new Follow(followId,follower,following,accountId,accountName,thumbnail));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			if(result!=null)
			{
				result.close();
			}
			if(preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if(connection!=null)
			{
				connection.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	public String totalFollower(String id)
	{
		AdminDAO adminDao = new AdminDAO();
		int count = adminDao.listFollower(id).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public String totalFollowing(String id)
	{
		AdminDAO adminDao = new AdminDAO();
		int count = adminDao.listFollowing(id).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public Account detailUser(String idUser)
	{
		Account account = null;
		Account detailAccount = null;
		AdminDAO adminDao = new AdminDAO();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DETAILUSER);
			preparedStatement.setString(1, idUser);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				String note = result.getString(9); 
				
				account = new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
			String totalFollower = adminDao.totalFollower(account.getId());
			String totalFollowing = adminDao.totalFollowing(account.getId());
			detailAccount = new Account(account.getId(),account.getName(),account.getPassword(),account.getBirthDate(),
					account.getGender(),account.getEmail(),account.getPhone(),account.getThumbnail(), account.getNote(),totalFollower,totalFollowing);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return detailAccount;
	}
	
	public String totalUser()
	{
		AdminDAO adminDao = new AdminDAO();
		int count = adminDao.listUser().size();
		String total = String.valueOf(count);
		return total;
	}
	
	public List<Video> listVideo()
	{
		List<Video> list = new ArrayList<>();
		
		List<Video> listVd = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.LISTVIDEO);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<Video> listVideo(String idUser)
	{
		List<Video> list = new ArrayList<>();
		
		List<Video> listVd = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.VIDEOUSER);
			preparedStatement.setString(1, idUser);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public Video detailVideo(String idVideo)
	{
		Video list = null;
		
		Video listVd = null;
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DETAILVIDEO);
			preparedStatement.setString(1, idVideo);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				list = new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount);
			}
			
				String totalHeart = heartDao.countHeart(list.getId());
				String totalComment = commentDao.countComment(list.getId());
				
				listVd = new Video(list.getId(),list.getDect(),list.getPostDate(),list.getPostTime(),list.getVideo(),list.getIdAccount(),
						list.getThumbnail(),list.getNameAccount(),totalHeart,totalComment);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return listVd;
	}
	
	public String totalVideo()
	{
		AdminDAO adminDao = new AdminDAO();
		int count = adminDao.listVideo().size();
		String total = String.valueOf(count);
		return total;
	}
	
	public List<Video> newListVideo(String today)
	{
		List<Video> list = new ArrayList<>();
		
		List<Video> listVd = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.NEWVIDEO);
			preparedStatement.setString(1, today);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String newVideo(String today)
	{
		AdminDAO adminDao = new AdminDAO();
		int count = adminDao.newListVideo(today).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public void deleteVideo(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DLETEVIDEO);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteUser(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETEUSER);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteHeart(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETEHEART);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteComment(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETECOMMENT);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteFollower(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETEFOLLOWER);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteFollowing(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETEFOLLOWING);
			
			preparedStatement.setString(1, id);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public List<Code> listCode()
	{
		List<Code> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.CODE);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String code = result.getString(1);
				String email = result.getString(2);
				list.add(new Code(code,email));
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public void deleteCode(String code)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.DELETECODE);
			
			preparedStatement.setString(1, code);

			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public List<Video> topTrending()
	{
		List<Video> list = new ArrayList<>();
		
		List<Video> listVd = new ArrayList<>();
		
		HeartDAO heartDao = new HeartDAO();
		CommentDAO commentDao = new CommentDAO();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.TOPTRENDING);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String postDate = result.getString(3);
				String postTime = result.getString(4);
				String video = result.getString(5);
				String thumbnail = result.getString(6);
				String idAccount = result.getString(7);
				String nameAccount = result.getString(8);
				LocalDate lcd = LocalDate.parse(postDate);
				LocalTime lct = LocalTime.parse(postTime);
				listVd.add(new Video(id,dect,lcd.format(date),lct.format(time),video,idAccount,thumbnail,nameAccount));
			}
			int top=0;
			for(Video itemVd : listVd)
			{
				String totalHeart = heartDao.countHeart(itemVd.getId());
				String totalComment = commentDao.countComment(itemVd.getId());
				top++;
				list.add(new Video(itemVd.getId(),itemVd.getDect(),itemVd.getPostDate(),itemVd.getPostTime(),itemVd.getVideo(),itemVd.getIdAccount(),
						itemVd.getThumbnail(),itemVd.getNameAccount(),totalHeart,totalComment, top));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<Account> search(String key)
	{
		List<Account> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(A_IndexQuery.SEARCHUSER);
			preparedStatement.setString(1,"0");
			preparedStatement.setString(2,"%"+key+"%");
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3);
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6);
				String phone = result.getString(7);
				String thumbnail = result.getString(8);
				String note = result.getString(9);
				list.add(new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
}
