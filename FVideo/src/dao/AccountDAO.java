package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Account;
import model.Image;
import model.Code;
import utils.AccountQuery;

public class AccountDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public void creatAccount(Account account)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.INSERT);
			//account.setName(new String(account.getName().getBytes("iso-8859-1"), "UTF-8"));
			//account.setGender(new String(account.getGender().getBytes("iso-8859-1"),"UTF-8"));
			account.setName(new String(account.getName().getBytes("UTF-8"), "UTF-8"));
			account.setGender(new String(account.getGender().getBytes("UTF-8"),"UTF-8"));
			preparedStatement.setString(1, account.getName());
			preparedStatement.setString(2, account.getPassword());
			preparedStatement.setString(3, account.getBirthDate());
			preparedStatement.setString(4, account.getGender());
			preparedStatement.setString(5, account.getEmail());
			preparedStatement.setString(6, account.getPhone());
			preparedStatement.setString(7, account.getThumbnail());
			preparedStatement.setString(8, account.getNote());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void creatCode(Code code)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.CREATECODE);
			preparedStatement.setString(1, code.getCode());
			preparedStatement.setString(2, code.getEmail());			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public Code getCode(String code,String email)
	{
		Code bitCode = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.GETCODE);
			preparedStatement.setString(1, code);
			preparedStatement.setString(2, email);
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String codeValue = result.getString(1);
				String emailValue = result.getString(2);
				
				bitCode = new Code(codeValue,emailValue);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return bitCode;
	}
	
	public void deleteCode(String code,String email)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.DELETECODE);
			preparedStatement.setString(1, code);
			preparedStatement.setString(2, email);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void changeImg(Image img,String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.CHANGEIMG);
			preparedStatement.setString(1, img.getImg());
			preparedStatement.setString(2, id);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public Account getValue(String idUser)
	{
		Account account = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.SELECT);
			preparedStatement.setString(1, idUser);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				String note = result.getString(9); 
				
				account = new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return account;
	}
	
	public Account profile(String idUser ,String idAccount)
	{
		FollowDAO followDao = new FollowDAO();
		Account account = null;
		Account profile = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.SELECT);
			preparedStatement.setString(1, idAccount);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3).trim();
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6).trim();
				String phone = result.getString(7).trim();
				String thumbnail = result.getString(8);
				String note = result.getString(9); 
				
				account = new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note);
			}
			String follow = "0";
			if(followDao.checkFollow(idUser, account.getId()) || idUser.equals(account.getId()))
			{
				follow = "1";
			}
			String totalFollower = followDao.totalFollower(account.getId(), idUser);
			String totalFollowing = followDao.totalFollowing(account.getId(), idUser);
			profile = new Account(account.getId(),account.getName(),account.getPassword(),account.getBirthDate(),
					account.getGender(),account.getEmail(),account.getPhone(),account.getThumbnail(),account.getNote(),follow,totalFollower,totalFollowing);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return profile;
	}
	
	public List<Account> listAccount()
	{
		List<Account> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.LISTACCOUNT);
			preparedStatement.setString(1,"0");
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String password = result.getString(3);
				String birthDate = result.getString(4);
				String gender = result.getString(5);
				String email = result.getString(6);
				String phone = result.getString(7);
				String thumbnail = result.getString(8);
				String note = result.getString(9);
				list.add(new Account(id,name,password,birthDate,gender,email,phone,thumbnail,note));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public void updateAccount(Account account, String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.UPDATE);
			account.setName(new String(account.getName().getBytes("UTF-8"), "UTF-8"));
			account.setGender(new String(account.getGender().getBytes("UTF-8"),"UTF-8"));
			preparedStatement.setString(1, account.getName());
			preparedStatement.setString(2, account.getPassword());
			preparedStatement.setString(3, account.getBirthDate());
			preparedStatement.setString(4, account.getGender());
			preparedStatement.setString(5, account.getEmail());
			preparedStatement.setString(6, account.getPhone());
			preparedStatement.setString(7, account.getThumbnail());
			preparedStatement.setString(8, account.getNote());
			preparedStatement.setString(9, id);
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public boolean checkMail(String mail)
	{
		Account account = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.CHECKMAIL);
			preparedStatement.setString(1, mail);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String email = result.getString(1);
				
				account = new Account(email);
			}
			
			if(account!=null)
			{
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public void updatePassword(Account account, String id, String email)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.UPDATEPASS);

			preparedStatement.setString(1, account.getPassword());
			preparedStatement.setString(2, account.getNote());
			preparedStatement.setString(3, id);
			preparedStatement.setString(4, email);
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void forgetPassword(Account account)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(AccountQuery.FORGETPASS);

			preparedStatement.setString(1, account.getPassword());
			preparedStatement.setString(2, account.getNote());
			preparedStatement.setString(3, account.getEmail());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
