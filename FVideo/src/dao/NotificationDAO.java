package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Notification;
import utils.NotificationQuery;

public class NotificationDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public void addNotification(Notification notification)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.ADD);
			notification.setContent(new String(notification.getContent().getBytes("UTF-8"),"UTF-8"));
			preparedStatement.setString(1, notification.getNotificationer());
			preparedStatement.setString(2, notification.getContent());
			preparedStatement.setString(3, notification.getDate());
			preparedStatement.setString(4, notification.getTime());
			preparedStatement.setString(5, notification.getStatus());
			preparedStatement.setString(6, notification.getIdVideo());
			preparedStatement.setString(7, notification.getIdAccount());
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public List<Notification> listNotification(String userId)
	{
		List<Notification> list = new ArrayList<>();
		
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.LISTNOTIFICATION);
			preparedStatement.setString(1, userId);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String notificationer = result.getString(2);
				String content = result.getString(3);
				String datent = result.getString(4);
				String timent = result.getString(5);
				String status = result.getString(6);
				String idVideo = result.getString(7);
				String idAccount = result.getString(8);
				String thumbnail = result.getString(9);
				String accountName = result.getString(10);
				
				LocalDate lcd = LocalDate.parse(datent);
				LocalTime lct = LocalTime.parse(timent);
				
				list.add(new Notification(id,notificationer,content,lcd.format(date),lct.format(time),status,idVideo,idAccount,thumbnail,accountName));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(result!=null)
				{
					result.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<Notification> listNotification(String userId, String statusNt)
	{
		List<Notification> list = new ArrayList<>();
		
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.COUNTNOTIFICATION);
			preparedStatement.setString(1, userId);
			preparedStatement.setString(2, statusNt);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String notificationer = result.getString(2);
				String content = result.getString(3);
				String datent = result.getString(4);
				String timent = result.getString(5);
				String status = result.getString(6);
				String idVideo = result.getString(7);
				String idAccount = result.getString(8);
				
				LocalDate lcd = LocalDate.parse(datent);
				LocalTime lct = LocalTime.parse(timent);
				
				list.add(new Notification(id,notificationer,content,lcd.format(date),lct.format(time),status,idVideo,idAccount));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(result!=null)
				{
					result.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String countNotification(String userId, String statusNt)
	{
		NotificationDAO notificationDao = new NotificationDAO();
		int count = notificationDao.listNotification(userId, statusNt).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public void updateNotification(String setValue, String getValue, String idAccount)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.UPDATENOTIFICATION);
			preparedStatement.setString(1, setValue);
			preparedStatement.setString(2, getValue);
			preparedStatement.setString(3, idAccount);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void deleteNotification(String id)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.DELETENOTIFICATION);
			preparedStatement.setString(1, id);
			
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public boolean checkNotification(String idAccount, String idVideo)
	{
		Notification noti = null;
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(NotificationQuery.CHECKNOTIFICATION);
			preparedStatement.setString(1, idAccount);
			preparedStatement.setString(2, idVideo);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String notificationer = result.getString(2);
				String content = result.getString(3);
				String datent = result.getString(4);
				String timent = result.getString(5);
				String status = result.getString(6);
				String video = result.getString(7);
				String account = result.getString(8);
				
				noti = new Notification(id,notificationer,content,datent,timent,status,video,account);
			}
			
			if(noti!=null && noti.getContent().equals("Đã thích video của bạn"))
			{
				return true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(result!=null)
				{
					result.close();
				}
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
}
