package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Comment;
import utils.CommentQuery;

public class CommentDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	
	public void addComment(Comment comment)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(CommentQuery.INSERT);
			comment.setContent(new String(comment.getContent().getBytes("iso-8859-1"),"UTF-8"));
			preparedStatement.setString(1, comment.getIdAccount());
			preparedStatement.setString(2, comment.getIdVideo());
			preparedStatement.setString(3, comment.getDate());
			preparedStatement.setString(4, comment.getTime());
			preparedStatement.setString(5, comment.getContent());
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public List<Comment> listComment(String idVideo)
	{
		List<Comment> list = new ArrayList<>();
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(CommentQuery.LISTCOMMENT);
			preparedStatement.setString(1, idVideo);
			result = preparedStatement.executeQuery();
			while(result.next())
			{
				String id = result.getString(1);
				String account = result.getString(2);
				String video = result.getString(3);
				String getDate = result.getString(4);
				String getTime = result.getString(5);
				String content = result.getString(6);
				String name = result.getString(7);
				String thumbnail = result.getString(8);
				LocalDate lcd = LocalDate.parse(getDate);
				LocalTime lct = LocalTime.parse(getTime);
				list.add(new Comment(id,account,video,lcd.format(date),lct.format(time),content,name,thumbnail));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String countComment(String idVideo)
	{
		CommentDAO commentDao = new CommentDAO();
		int count = commentDao.listComment(idVideo).size();
		String comment = String.valueOf(count);
		return comment;
	}
	
	public void deleteComment(String idComment)
	{
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(CommentQuery.DELETE);
			preparedStatement.setString(1, idComment);
			preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
