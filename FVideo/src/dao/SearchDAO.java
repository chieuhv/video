package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnection;
import model.Search;
import model.Video;
import utils.SearchQuery;

public class SearchDAO {
	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet result;
	

	public List<Video> listVideo(String idAccount)
	{
		List<Video> list = new ArrayList<>();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(SearchQuery.LISTVIDEO);
			preparedStatement.setString(1, idAccount);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String dect = result.getString(2);
				String date = result.getString(3);
				String time = result.getString(4);
				String video = result.getString(5);
				String account = result.getString(6);
				
				list.add(new Video(id,dect,date,time,video,account));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String countVideo(String idAccount)
	{
		SearchDAO searchDao = new SearchDAO();
		int count = searchDao.listVideo(idAccount).size();
		String total = String.valueOf(count);
		return total;
	}
	
	public List<Search> resultSearch(String key, String note, String idUser)
	{
		List<Search> listAccount = new ArrayList<>();
		List<Search> list = new ArrayList<>();
		SearchDAO searchDao = new SearchDAO();
		FollowDAO followDao = new FollowDAO();
		try {
			connection = DBConnection.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(SearchQuery.SEARCH);
			preparedStatement.setString(1, "%"+key+"%");
			preparedStatement.setString(2, note);
			
			result = preparedStatement.executeQuery();
			
			while(result.next())
			{
				String id = result.getString(1);
				String name = result.getString(2);
				String thumbnail = result.getString(3);
				
				listAccount.add(new Search(id,name,thumbnail));
			}
			for(Search item : listAccount)
			{
				String follow = followDao.totalFollower(item.getId(), idUser);
				String video = searchDao.countVideo(item.getId());
				list.add(new Search(item.getId(),item.getName(),item.getThumbnail(),follow,video));
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if(connection!=null)
				{
					connection.close();
				}
				if(preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if(result!=null)
				{
					result.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}	
	
}
