package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import model.SendCode;
import utils.Message;


@WebServlet("/account")
public class CreateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CreateAccount() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.UPDATEACCOUNT);
			request.setAttribute("bg", Message.BG);
		}
		RequestDispatcher red = request.getRequestDispatcher("/view/Account.jsp");
		red.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String date = request.getParameter("date");
		String gender = request.getParameter("gender");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String thumbnail = "";
		String note = "0";
		
		if(gender.equalsIgnoreCase("Nam"))
		{
			thumbnail = "upload/man.jpg";
		}
		else
		{
			thumbnail = "upload/woman.jpg";
		}
		
		AccountDAO accountDao = new AccountDAO();
		
		if(accountDao.checkMail(email))
		{
			HttpSession sessCreate = request.getSession();
			Account account = new Account(name,password,date,gender,email,phone,thumbnail,note);
			sessCreate.setAttribute("account", account);
			String createCode = SendCode.randomCode();
			accountDao.creatCode(new Code(createCode,account.getEmail()));
			SendCode.senMail(createCode, account.getEmail());
			
			response.sendRedirect(request.getContextPath()+"/code");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/account?message=emailexist&alert=bg");
		}	
	}

}
