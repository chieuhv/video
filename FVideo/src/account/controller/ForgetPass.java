package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import utils.Message;

@WebServlet("/forgetpass")
public class ForgetPass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ForgetPass() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.CODEFAILDE);
			request.setAttribute("alert", Message.BG);
		}
		RequestDispatcher de = request.getRequestDispatcher("/view/CheckForgetPassword.jsp");
		de.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountDAO accountDao = new AccountDAO();
		
		String code = request.getParameter("code");
		HttpSession forget = request.getSession();
		Account account = (Account) forget.getAttribute("password");
		
		Code codeValue = accountDao.getCode(code, account.getEmail());
		
		if(codeValue!=null)
		{
			accountDao.forgetPassword(new Account(account.getPassword(),account.getEmail(),account.getNote()));
			forget.invalidate();
			response.sendRedirect(request.getContextPath()+"/login");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/forgetpass?message=codefailed&alert=bg");
		}
		accountDao.deleteCode(code, account.getEmail());
	}

}
