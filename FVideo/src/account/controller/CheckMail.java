package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import model.SendCode;
import utils.Message;

@WebServlet("/checkmail")
public class CheckMail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CheckMail() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.CHECKMAIL);
			request.setAttribute("alert", Message.BG);
		}
		RequestDispatcher de = request.getRequestDispatcher("/view/ForgetPassword.jsp");
		de.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Account account = new Account(password,email,"0");
		
		if(!accountDao.checkMail(email))
		{
			HttpSession forget = request.getSession();
			forget.setAttribute("password", account);
			
			String createCode = SendCode.randomCode();
			accountDao.creatCode(new Code(createCode,email));
			SendCode.senMail(createCode, email);
			
			response.sendRedirect(request.getContextPath()+"/forgetpass");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/checkmail?message=emailnotexist&alert=bg");
		}
	}

}
