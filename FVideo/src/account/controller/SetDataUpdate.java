package account.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import model.Login;
import model.SendCode;

@WebServlet("/video/setdata")
public class SetDataUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public SetDataUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		String name = request.getParameter("name");
		String password = login.getPassword();
		String date = request.getParameter("date");
		String gender = request.getParameter("gender");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String thumbnail = login.getThumbnail();
		String note = login.getNote();
		
		if(accountDao.checkMail(email) || email.equals(login.getEmail()))
		{
			Account account = new Account(name,password,date,gender,email,phone,thumbnail,note);
			HttpSession update = request.getSession();
			update.setAttribute("update", account);
			String createCode = SendCode.randomCode();
			accountDao.creatCode(new Code(createCode,login.getEmail()));
			SendCode.senMail(createCode, login.getEmail());
			response.sendRedirect(request.getContextPath()+"/video/updateaccount");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/myaccount?message=emailexist&alert=bg");
		}
	}

}
