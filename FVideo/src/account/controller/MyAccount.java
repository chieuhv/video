package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import model.Account;
import model.Login;
import utils.Message;

@WebServlet("/video/myaccount")
public class MyAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public MyAccount() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("mess", Message.UPDATEACCOUNT);
			request.setAttribute("bg", Message.BG);
		}
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		Account account = accountDao.getValue(login.getId());
		request.setAttribute("account", account);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/MyAccount.jsp");
		de.forward(request, response);
	}

}
