package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import utils.Message;

@WebServlet("/code")
public class DataCreateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DataCreateAccount() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.CODEFAILDE);
			request.setAttribute("alert", Message.BG);
		}
		
		RequestDispatcher de = request.getRequestDispatcher("/view/Code.jsp");
		de.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String code = request.getParameter("code");
		
		HttpSession sessCreate = request.getSession();
		Account account = (Account)sessCreate.getAttribute("account");
	
		String thumbnail = "";
		if(account.getGender().equalsIgnoreCase("Nam"))
		{
			thumbnail = "upload/man.jpg";
		}
		else
		{
			thumbnail = "upload/woman.jpg";
		}
		Code codeValue = accountDao.getCode(code, account.getEmail());
		
		if(codeValue!=null)
		{
			accountDao.creatAccount(new Account(account.getName(), account.getPassword(),account.getBirthDate(),account.getGender(),account.getEmail(),account.getPhone(),thumbnail,"0"));
			sessCreate.invalidate();
			response.sendRedirect(request.getContextPath()+"/login");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/code?message=codefailed&alert=bg");
		}
		accountDao.deleteCode(code, account.getEmail());
	}

}
