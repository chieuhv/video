package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.LoginDAO;
import model.Account;
import model.Code;
import model.Login;
import utils.Message;

@WebServlet("/video/updateaccount")
public class UpdateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public UpdateAccount() {
        super();
       
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String message = request.getParameter("message");
    	String alert = request.getParameter("alert");
    	
    	if(message!=null && alert!=null)
    	{
	    	request.setAttribute("message", Message.CODEFAILDE);
			request.setAttribute("alert", Message.BG);
    	}
    	RequestDispatcher de = request.getRequestDispatcher("/view/CheckUpdateAccount.jsp");
		de.forward(request, response);
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountDAO accountDao = new AccountDAO();
		LoginDAO loginDao = new LoginDAO();
		
		String code = request.getParameter("code");
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		HttpSession update = request.getSession();
		Account account = (Account)update.getAttribute("update");
		
		Code codeValue = accountDao.getCode(code, login.getEmail());
		
		if(codeValue!=null)
		{			
			accountDao.updateAccount(new Account(account.getName(),login.getPassword(),account.getBirthDate(),account.getGender(),
					account.getEmail(),account.getPhone(),login.getThumbnail(),login.getNote()), login.getId());
			update.invalidate();
			HttpSession sessionNew = request.getSession();
			Login loginNew = loginDao.getValue(account.getEmail(), login.getPassword());
			sessionNew.setAttribute("user", loginNew);
			response.sendRedirect(request.getContextPath()+"/video/myaccount");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/updateaccount?message=codefailed&alert=bg");
		}
		accountDao.deleteCode(code, login.getEmail());
		
	}

}
