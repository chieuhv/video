package account.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dao.AccountDAO;
import dao.NotificationDAO;
import model.Account;
import model.Image;
import model.Login;
import utils.Message;

@WebServlet("/video/change")
public class ChangeImg extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public ChangeImg() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.UPLOAD);
			request.setAttribute("alert", Message.BG);
		}
		AccountDAO accountDao = new AccountDAO();
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/ChangeImg.jsp");
		de.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
				
		final int MEMORY_THRESHOLD = 1024 * 1024 * 3;  // 3MB
		final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
		final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

		String UPLOAD_DIRECTORY = "upload";
   
		DiskFileItemFactory factory = new DiskFileItemFactory();

		factory.setSizeThreshold(MEMORY_THRESHOLD);

		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);

		upload.setFileSizeMax(MAX_FILE_SIZE);

		upload.setSizeMax(MAX_REQUEST_SIZE);
      
		String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
           uploadDir.mkdir();
		}
		boolean flag = false;
		String imgFileName = "";
		String filePath = "";
		try {
           List<FileItem> formItems = upload.parseRequest(request);
           if (formItems != null && formItems.size() > 0) {
               for (FileItem item : formItems) {
                   if (!item.isFormField()) {
                       String fileName = new File(item.getName()).getName();
                       filePath = uploadPath + File.separator + fileName;
                       
                       File storeFile = new File(filePath);
                       item.write(storeFile);
                       flag = true;
                       imgFileName = UPLOAD_DIRECTORY + "/" + fileName;
                   }
               }
           }
		} catch (Exception ex) {
			response.sendRedirect(request.getContextPath()+"/video/change?message=failed&alert=bg");
		}
		if(flag)
		{
			HttpSession session = request.getSession();
			Login login = (Login) session.getAttribute("user");

			accountDao.changeImg(new Image(imgFileName), login.getId());
			Account img = accountDao.getValue(login.getId());
			request.setAttribute("thumbnail", img);
			RequestDispatcher de = request.getRequestDispatcher("/view/ChangeImg.jsp");
			de.forward(request, response);
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/change?message=failed&alert=bg");
		}
	}

}
