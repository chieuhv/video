package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Code;
import model.Login;
import utils.Message;

@WebServlet("/video/updatepass")
public class UpdatePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public UpdatePassword() {
        super();
       
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String message = request.getParameter("message");
    	String alert = request.getParameter("alert");
    	if(message!=null && alert!=null)
    	{
    		request.setAttribute("message", Message.CODEFAILDE);
    		request.setAttribute("alert", Message.BG);
    	}
    	
		RequestDispatcher de = request.getRequestDispatcher("/view/CheckChangePass.jsp");
		de.forward(request, response);
    }
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		String code = request.getParameter("code");
		
		Code codeValue = accountDao.getCode(code, login.getEmail());
		
		if(codeValue!=null)
		{
			HttpSession change = request.getSession();
			Account account = (Account) change.getAttribute("change");
			
			accountDao.updatePassword(new Account(account.getPassword(),account.getNote()), login.getId(), login.getEmail());
			change.invalidate();
			session.invalidate();
			
			response.sendRedirect(request.getContextPath()+"/login");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/updatepass?message=codefailed&alert=bg");
		}
		accountDao.deleteCode(code, login.getEmail());
	}

}
