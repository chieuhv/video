package account.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import model.Account;
import model.Code;
import model.Login;
import model.SendCode;
import utils.Message;

@WebServlet("/video/changepass")
public class ChangePass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ChangePass() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("mess", Message.CHECKPASS);
			request.setAttribute("bg", Message.BG);
		}
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/UpdatePassword.jsp");
		de.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String email = request.getParameter("email");
		String passold = request.getParameter("passold");
		String passnew = request.getParameter("passnew");
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account account = new Account(passnew,login.getNote());
		
		if(email.equals(login.getEmail()) && passold.equals(login.getPassword()))
		{
			
			HttpSession change = request.getSession();
			change.setAttribute("change", account);
			
			String createCode = SendCode.randomCode();
			accountDao.creatCode(new Code(createCode,login.getEmail()));
			SendCode.senMail(createCode, login.getEmail());
			
			response.sendRedirect(request.getContextPath()+"/video/updatepass");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/changepass?message=notexist&alert=bg");
		}
	}

}
