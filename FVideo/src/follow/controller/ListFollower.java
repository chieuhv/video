package follow.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.FollowDAO;
import dao.NotificationDAO;
import model.Account;
import model.Follow;
import model.Login;

@WebServlet("/video/follower")
public class ListFollower extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListFollower() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FollowDAO followDao = new FollowDAO();
		AccountDAO accountDao = new AccountDAO();
		
		String id = request.getParameter("id");
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		Account profile = accountDao.profile(login.getId(), id);
		request.setAttribute("profile", profile);
		
		List<Follow> listFollower = followDao.listFollower(id,login.getId());
		request.setAttribute("follower", listFollower);
		
		NotificationDAO notificationDao = new NotificationDAO();
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/Follower.jsp");
		de.forward(request, response);
	}
}
