package follow.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FollowDAO;

@WebServlet("/video/deletefollow")
public class DeleteFollow extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public DeleteFollow() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FollowDAO followDao = new FollowDAO();
		String follower = request.getParameter("follower");
		String following = request.getParameter("following");
		followDao.deleteFollow(follower,following);
	}

}
