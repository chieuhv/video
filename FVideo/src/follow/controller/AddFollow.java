package follow.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FollowDAO;
import model.Follow;

@WebServlet("/video/addfollow")
public class AddFollow extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public AddFollow() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FollowDAO followDao = new FollowDAO();
		String followerid = request.getParameter("followerid");
		String followingid = request.getParameter("followingid");
		String accountid = request.getParameter("accountid");
		
		followDao.addFollow(new Follow(followerid,followingid,accountid));
	}
}
