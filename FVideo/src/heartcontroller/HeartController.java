package heartcontroller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.HeartDAO;
import dao.NotificationDAO;
import model.Heart;
import model.Notification;

@WebServlet("/video/heart")
public class HeartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public HeartController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HeartDAO heartDao = new HeartDAO();
		NotificationDAO notificationDao = new NotificationDAO();
		
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		String idAccount = request.getParameter("idAccount");
		String idVideo = request.getParameter("idVideo");
		String account = request.getParameter("account");
		
		Date date = new Date();
		String time = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(time);
		String datetime = sdf.format(date);
		
		LocalTime lct = LocalTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		String hour = lct.format(dtf);
		
		if(heartDao.checkHeart(idAccount, idVideo))
		{
			heartDao.deleteHeart(idAccount, idVideo);
		}
		else
		{
			heartDao.addHeart(new Heart(idAccount,idVideo));
			String content = "Đã thích video của bạn";
			if(!notificationDao.checkNotification(idAccount, idVideo))
			{
				if(!idAccount.equals(account))
				{
					notificationDao.addNotification(new Notification(idAccount,content,datetime,hour,"0",idVideo,account));
				}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
