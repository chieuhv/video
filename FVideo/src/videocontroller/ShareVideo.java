package videocontroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.VideoDAO;
import model.Video;

@WebServlet("/sharevideo")
public class ShareVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ShareVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		VideoDAO videoDao = new VideoDAO();
		
		String id = request.getParameter("id");
		Video video = videoDao.shareVideo(id);
		request.setAttribute("share", video);
		RequestDispatcher de = request.getRequestDispatcher("/view/Share.jsp");
		de.forward(request, response);
	}

}
