package videocontroller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dao.VideoDAO;
import model.Video;

@WebServlet("/video/postvideo")
public class AddVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public AddVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		VideoDAO videoDao = new VideoDAO();
		
		final int MEMORY_THRESHOLD = 1024 * 1024 * 3;  // 3MB
		final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
		final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

		String UPLOAD_DIRECTORY = "media";
   
		DiskFileItemFactory factory = new DiskFileItemFactory();

		factory.setSizeThreshold(MEMORY_THRESHOLD);

		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);

		upload.setFileSizeMax(MAX_FILE_SIZE);

		upload.setSizeMax(MAX_REQUEST_SIZE);
      
		String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
           uploadDir.mkdir();
		}
		boolean flag = false;
		String imgFileName = "";
		try {
           List<FileItem> formItems = upload.parseRequest(request);
           if (formItems != null && formItems.size() > 0) {
               for (FileItem item : formItems) {
            	   if (item.isFormField()) {
                       String name = item.getFieldName();
                       String value = item.getString();
                       request.setAttribute(name, value);
                   }
                   else {
                       String fileName = new File(item.getName()).getName();
                       String filePath = uploadPath + File.separator + fileName;
                       File storeFile = new File(filePath);
                       item.write(storeFile);
                       flag = true;
                       imgFileName = UPLOAD_DIRECTORY + "/" + fileName;
                   }
               }
           }
		} catch (Exception ex) {
			response.sendRedirect(request.getContextPath()+"/video/home?message=failed&alert=bg");
		}
		if(flag)
		{
			Date date = new Date();
			String time = "yyyy/MM/dd";
			SimpleDateFormat sdf = new SimpleDateFormat(time);
			String datetime = sdf.format(date);
			
			LocalTime lct = LocalTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
			String hour = lct.format(dtf);
			
			String id  = request.getParameter("id");
			String dect = (String) request.getAttribute("dect");
			videoDao.addVideo(new Video(dect,datetime,hour,imgFileName,id));
			
			response.sendRedirect(request.getContextPath()+"/video/home");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/video/home?message=failed&alert=bg");
		}
	}

}
