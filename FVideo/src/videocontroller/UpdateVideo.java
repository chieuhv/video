package videocontroller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.VideoDAO;
import model.Login;
import model.Video;

@WebServlet("/video/updatevideo")
public class UpdateVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		VideoDAO videoDao = new VideoDAO();
		
		String id = request.getParameter("id");
		String dect = request.getParameter("dect");
		
		HttpSession session = request.getSession();
		Login login = (Login)session.getAttribute("user");
		
		Date date = new Date();
		String time = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(time);
		String datetime = sdf.format(date);
		
		LocalTime lct = LocalTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		String hour = lct.format(dtf);
		
		videoDao.updateVideo(new Video(dect,datetime,hour), id, login.getId());
		
		response.sendRedirect(request.getContextPath()+"/video/profile?id="+login.getId());
	}

}
