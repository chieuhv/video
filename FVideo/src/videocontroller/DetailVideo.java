package videocontroller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import dao.VideoDAO;
import model.Account;
import model.Login;
import model.Video;

@WebServlet("/video/detailvideo")
public class DetailVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DetailVideo() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		VideoDAO videoDao = new VideoDAO();
		AccountDAO accountDao = new AccountDAO();
		NotificationDAO notificationDao = new NotificationDAO();
		
		String idVideo = request.getParameter("idvideo");
		
		HttpSession session = request.getSession();
		Login login = (Login)session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		Video video = videoDao.detailVideo(idVideo, login.getId());
		request.setAttribute("video",video);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/UpdateVideo.jsp");
		de.forward(request, response);
	}
}
