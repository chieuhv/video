package videocontroller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.VideoDAO;
import model.Login;

@WebServlet("/video/deletevideo")
public class DeleteVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public DeleteVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		VideoDAO videoDao = new VideoDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login)session.getAttribute("user");
		
		String id = request.getParameter("lastId");
		videoDao.deleteVideo(id, login.getId());
		
		response.sendRedirect(request.getContextPath()+"/video/profile?id="+login.getId());
	}
}
