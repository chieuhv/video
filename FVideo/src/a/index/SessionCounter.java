package a.index;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;

import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionCounter implements HttpSessionListener {
	
	private static int counter = 0;
	
	public static int getActiveSessionNumber() {
        return counter;
    }
	
    public void sessionCreated(HttpSessionEvent event)  { 
    	counter++;
    }
    
    public void sessionDestroyed(HttpSessionEvent event)  { 
    	counter--;
    }
}
