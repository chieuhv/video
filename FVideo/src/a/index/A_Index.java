package a.index;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;

@WebServlet("/manager/admin")
public class A_Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public A_Index() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date date = new Date();
		String time = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(time);
		String datetime = sdf.format(date);
		
		AdminDAO adminDao = new AdminDAO();
		request.setAttribute("user", adminDao.totalUser());
		request.setAttribute("counter", SessionCounter.getActiveSessionNumber());
		request.setAttribute("video", adminDao.totalVideo());
		request.setAttribute("newvideo", adminDao.newVideo(datetime));
		
		RequestDispatcher de = request.getRequestDispatcher("/view/A-Index.jsp");
		de.forward(request, response);
	}

}
