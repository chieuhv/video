package a.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.LoginDAO;
import model.Code;
import model.Login;
import model.MailLogin;
import utils.Message;

@WebServlet("/adminchecklogin")
public class A_CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public A_CheckLogin() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String alert = request.getParameter("alert");
		if(message!=null && alert!=null)
		{
			request.setAttribute("message", Message.CODEFAILDE);
			request.setAttribute("alert", Message.BG);
		}
		RequestDispatcher red = request.getRequestDispatcher("/view/A-CheckLogin.jsp");
		red.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDAO loginDao = new LoginDAO();
		String code = request.getParameter("code");
		Code bitCode = loginDao.getCode(code);

		if(bitCode!=null)
		{
			HttpSession sessionLogin = request.getSession();
			Login getValue = (Login)sessionLogin.getAttribute("login");
			
			Login login = loginDao.getValue(getValue.getEmail(), getValue.getPassword());
			sessionLogin.invalidate();
			
			HttpSession session = request.getSession();
			session.setAttribute("admin", login);
			
			loginDao.deleteCode(code);
			MailLogin.senMail(bitCode.getEmail());
			response.sendRedirect(request.getContextPath()+"/manager/admin");
		}
		else
		{
			response.sendRedirect(request.getContextPath()+"/adminchecklogin?message=codefailed&alert=bg");
		}
	}

}
