package a.search;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Account;

@WebServlet("/manager/adminsearch")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public Search() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		String key = request.getParameter("key");
		List<Account> listAccount = adminDao.search(key);
		request.setAttribute("user", listAccount);
		RequestDispatcher de = request.getRequestDispatcher("/view/A-Search.jsp");
		de.forward(request, response);
	}
}
