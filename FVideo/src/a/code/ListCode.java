package a.code;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Code;

@WebServlet("/manager/listcode")
public class ListCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListCode() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		List<Code> list = adminDao.listCode();
		request.setAttribute("code", list);
		RequestDispatcher de = request.getRequestDispatcher("/view/A-Code.jsp");
		de.forward(request, response);
	}
}
