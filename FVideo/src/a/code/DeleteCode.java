package a.code;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;

@WebServlet("/manager/deletecode")
public class DeleteCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public DeleteCode() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		String code = request.getParameter("lastId");
		adminDao.deleteCode(code);
		response.sendRedirect(request.getContextPath()+"/manager/listcode");
	}
}
