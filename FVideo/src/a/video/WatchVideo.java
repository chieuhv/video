package a.video;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Video;

@WebServlet("/manager/watch")
public class WatchVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public WatchVideo() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		String idVideo = request.getParameter("id");
		Video video = adminDao.detailVideo(idVideo);
		request.setAttribute("video", video);
		RequestDispatcher de = request.getRequestDispatcher("/view/A-DetailVideo.jsp");
		de.forward(request, response);
	}
}
