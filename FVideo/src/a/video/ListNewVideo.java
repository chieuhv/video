package a.video;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Video;

@WebServlet("/manager/newlistvideo")
public class ListNewVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListNewVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		Date date = new Date();
		String time = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(time);
		String datetime = sdf.format(date);
		
		List<Video> listVideo = adminDao.newListVideo(datetime);
		request.setAttribute("video", listVideo);
		RequestDispatcher de = request.getRequestDispatcher("/view/A-NewListVideo.jsp");
		de.forward(request, response);
	}

}
