package a.video;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Video;

@WebServlet("/manager/listvideo")
public class ListVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public ListVideo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();

		List<Video> listVideo = adminDao.listVideo();
		request.setAttribute("video", listVideo);
		RequestDispatcher de = request.getRequestDispatcher("/view/A-ListVideo.jsp");
		de.forward(request, response);
	}
}
