package a.users;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;

@WebServlet("/manager/deleteuser")
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteUser() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		String id = request.getParameter("lastId");
		adminDao.deleteHeart(id);
		adminDao.deleteComment(id);
		adminDao.deleteFollower(id);
		adminDao.deleteFollowing(id);
		adminDao.deleteUser(id);
		response.sendRedirect(request.getContextPath()+"/manager/listuser");
	}

}
