package a.users;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Account;

@WebServlet("/manager/listuser")
public class ListUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListUsers() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		List<Account> list = adminDao.listUser();
		request.setAttribute("user", list);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/A-ListUser.jsp");
		de.forward(request, response);
	}
}
