package a.users;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Login;

@WebServlet("/manager/profileadmin")
public class DetailAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public DetailAdmin() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login)session.getAttribute("admin");
		Account account = accountDao.getValue(login.getId());
		request.setAttribute("account", account);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/A-DetailAdmin.jsp");
		de.forward(request, response);
	}
}
