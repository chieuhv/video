package a.users;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import model.Account;
import model.Video;

@WebServlet("/manager/detailsuser")
public class DetailUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DetailUser() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminDAO adminDao = new AdminDAO();
		
		String id = request.getParameter("id");
		Account account = adminDao.detailUser(id);
		request.setAttribute("account", account);
		
		List<Video> listVideo = adminDao.listVideo(id);
		request.setAttribute("video", listVideo);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/A-DetailUser.jsp");
		de.forward(request, response);
	}
}
