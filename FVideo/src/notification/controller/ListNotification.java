package notification.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import dao.NotificationDAO;
import model.Account;
import model.Login;
import model.Notification;

@WebServlet("/video/listnotification")
public class ListNotification extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public ListNotification() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NotificationDAO notificationDao = new NotificationDAO();
		AccountDAO accountDao = new AccountDAO();
		
		HttpSession session = request.getSession();
		Login login = (Login) session.getAttribute("user");
		
		Account image = accountDao.getValue(login.getId());
		request.setAttribute("thumbnail", image);
		
		List<Notification> list = notificationDao.listNotification(login.getId());
		request.setAttribute("noti", list);
		
		String totalNotification = notificationDao.countNotification(login.getId(), "0");
		request.setAttribute("newnoti", totalNotification);
		
		RequestDispatcher de = request.getRequestDispatcher("/view/Notification.jsp");
		de.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
