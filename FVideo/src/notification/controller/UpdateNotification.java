package notification.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.NotificationDAO;

@WebServlet("/video/updatenotication")
public class UpdateNotification extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public UpdateNotification() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NotificationDAO notificationDao = new NotificationDAO();
		String idAccount = request.getParameter("id");
		notificationDao.updateNotification("1", "0", idAccount);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
