package notification.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.NotificationDAO;

@WebServlet("/video/deletenotification")
public class DeleteNotification extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteNotification() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NotificationDAO notificationDao = new NotificationDAO();
		String id = request.getParameter("id");
		notificationDao.deleteNotification(id);
	}
}
